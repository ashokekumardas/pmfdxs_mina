﻿//using System;
//using System.Data;
//using System.Data.SqlClient;

namespace PurpleMoorFDService.DBManager
{
    public partial class SqlDataHelper
    {
//        public static Guid GetGuid(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return Guid.Empty; }
//            return (Guid)rdr[index];
//        }

//        public static string GetString(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return string.Empty; }
//            return (string)rdr[index];
//        }

//        public static char GetChar(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return ' '; }
//            return Convert.ToChar(rdr[index]);
//        }

//        public static int GetInt(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return 0; }
//            return Convert.ToInt32(rdr[index]);
//        }

//        public static Int64 GetBigInt(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return 0; }
//            return Convert.ToInt64(rdr[index]);
//        }
//        public static Int16 GetShort(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return 0; }
//            return Convert.ToInt16(rdr[index]);
//        }

//        public static DateTime GetDateTime(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//            { return DateTime.MinValue; }
//            return Convert.ToDateTime(rdr[index]);
//        }

//        public static TimeSpan GetTime(IDataReader rdr, string ColumnName)
//        {
//            int index = rdr.GetOrdinal(ColumnName);
//            if (rdr.IsDBNull(index) == true)
//                return TimeSpan.MinValue;
//            return (TimeSpan)rdr[index];
//        }

//        public static SqlConnection CreateConnection(string ConnectionString)
//        {
//            SqlConnection sqlConn = new SqlConnection(ConnectionString);
//            try
//            {
//                sqlConn.Open();
//            }
//            catch (Exception e)
//            {

//            }
//            return sqlConn;
//        }
        public static string GetConnectionString()
        {
            string ConnStr = "";
            if (System.Configuration.ConfigurationManager.AppSettings["DatabasePath"] != null)
                ConnStr = System.Configuration.ConfigurationManager.AppSettings["DatabasePath"];
            return ConnStr;
        }
        public static string GetDatabaseType()
        {
            string strDBType = string.Empty;
            if (System.Configuration.ConfigurationManager.AppSettings["DatabaseType"] != null)
            {
                strDBType = System.Configuration.ConfigurationManager.AppSettings["DatabaseType"];
            }

            return strDBType;
        }

    }
}
