﻿using PMFDServiceDAL;
using PurpleMoorFDService.ProgramModule;
using System;

namespace PurpleMoorFDService.DBManager
{
    public class AttendanceManager
    {
        internal Attendance ProcessAttendanceDataFromJson(string AttendanceDataJson)  //convert json to job
        {
            Attendance Attn = new Attendance();
            try
            {
                ReceivedCommandData RCD = new Json().ConvertJSonToObject<ReceivedCommandData>(AttendanceDataJson);// convert Json to class object
                string DeviceSN = RCD.PARAM.sn;// get device serial no
                Attn.CPUID = DeviceSN;
                Attn.Command = 1002;  // JSON type data (command 1001 old format attendance data)
                Attn.CommandNo = 0;
                Attn.CommandData = AttendanceDataJson;
                Attn.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                Attn.SendTo = 'S';
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "ProcessAttendanceDataFromJson( " + ex.Message + ")"); }
            return Attn;
        }

        public Boolean Insert(Attendance job)
        {
            bool success=true;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                new PMFDServiceDAL.CommonDBLayer().InsertAttendanceData(strDatabaseType, ConnStr,job);
            }

            catch (Exception ex)
            {
                success = false;
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "AttendanceManager.Insert( "+job.CommandData.Substring(0,50) +" "+ex.Message+")" );
            }
            return success;
        }
    }
}
