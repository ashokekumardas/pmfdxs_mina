﻿using System.ServiceProcess;
using System.Threading;

namespace PurpleMoorFDService
{
    public partial class PurpleMoorFDService : ServiceBase
    {
        public PurpleMoorFDService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
          new ProgramModule.ListenClient().StartService();
            //lstclnt.StartService();
            //Thread oThread = new Thread(lstclnt.StartService);
            //oThread.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
