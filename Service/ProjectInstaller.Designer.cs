﻿namespace PurpleMoorFDService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PurpleMoorFDServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.PurpleMoorFDServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // PurpleMoorFDServiceProcessInstaller
            // 
            this.PurpleMoorFDServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.PurpleMoorFDServiceProcessInstaller.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PurpleMoorFDServiceInstaller});
            this.PurpleMoorFDServiceProcessInstaller.Password = null;
            this.PurpleMoorFDServiceProcessInstaller.Username = null;
            // 
            // PurpleMoorFDServiceInstaller
            // 
            this.PurpleMoorFDServiceInstaller.ServiceName = "PurpleMoorFDService";
            this.PurpleMoorFDServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PurpleMoorFDServiceProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller PurpleMoorFDServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller PurpleMoorFDServiceInstaller;
    }
}