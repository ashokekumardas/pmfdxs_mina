﻿using Com.FirstSolver.Splash;  //31-Mar-2021 // direct data entry to tblTattendanceJob , MSMQ only for raw file write
using Mina.Core.Session;
using Mina.Filter.Codec;
using Mina.Transport.Socket;
using PMFDServiceDAL;
using PurpleMoorFDService.DBManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Timers;

namespace PurpleMoorFDService.ProgramModule
{
    public class ListenClient
    {
        //********************************************
        private static int WM_QUERYENDSESSION = 0x11;
        // is the system wanting to shutdown
        private static bool systemShutdown = false;
        //*********************************************

        //private UdpClientPlus UdpServer = null;
        private string ProcessName = string.Empty;
        public static Timer timerDataMQ;
        public static Timer timerCleanFiles;
        // Serial number attribute key
        private static readonly AttributeKey KEY_SERIALNUMBER = new AttributeKey(typeof(ListenClient), "SerialNumber");

        /// <summary>
        /// Device communication character set code page is Simplified Chinese
        /// </summary>           
        private const int DeviceCodePage = 936;

        /// <summary>
        /// Whether the server is running
        /// </summary>
        private bool IsServerRunning = false;

        /// <summary>
        /// Listening server
        /// </summary>
        private AsyncSocketAcceptor TcpServer = null;
        private AsyncDatagramAcceptor UdpServer = null;
        /// <summary>
        /// Listening new device server 
        /// </summary>
        private AsyncSocketAcceptor TcpNewServer = null;
        private AsyncDatagramAcceptor UdpNewServer = null;
        List<Job> MasterListOfJobs = new List<Job>();
        const int conNoOfOldJobsToBeSentInOneSession = 20; // number of old jobs to be sent in one connection session
        const int conNoOfNewJobsToBeSentInOneSession = 20; // number of new jobs to be sent in one connection session
        private string strInsertFaceDataForNewDevice = "No";
        private string strInsertPostPhotoToDB = "Yes";// insert photo with attendance record to db
        public void StartService()
        {
            try
            {
                ProcessName = Process.GetCurrentProcess().ProcessName;
                string IPAddress = ReadSettings.getServerIP();
                string CMDPort = ReadSettings.getServerCMDPort();
                string HBport = ReadSettings.getServerHBPort();
                string CmdNewPort = ReadSettings.getServerCMDNewPort();
                string HBNewport = ReadSettings.getServerHBNewPort();
                strInsertFaceDataForNewDevice = ReadSettings.getFaceDeviceTemplateInsertSetting();// settings for face tempale insert to tblJob for VF1000
                strInsertPostPhotoToDB = ReadSettings.getFacePhotoInsertSetting();//settings for insert face photo to db table
                try
                {
                    StartServiceInPort(IPAddress, CMDPort, CmdNewPort);
                    StartUDPServiceInPort(IPAddress, HBport, HBNewport);// start udp service

                    /////////////////////////  check the MSMQ data read  /////////////////////
                    //HandleMSMQ HCMQ = new HandleMSMQ(ProcessName);
                    //timerDataMQ = new System.Timers.Timer(10000);
                    //timerDataMQ.Elapsed += new ElapsedEventHandler(HCMQ.ReadFromMessageQ);
                    //timerDataMQ.Interval = 5000;
                    //timerDataMQ.Enabled = true;
                    /////////////////////////  clean the old log & raw files //////////////////
                    EventLogger EL = new EventLogger();
                    timerCleanFiles = new Timer(12 * 60 * 60 * 1000);
                    timerCleanFiles.Elapsed += new ElapsedEventHandler(EL.CleanLogFiles);
                    timerCleanFiles.Enabled = true;

                    System.Threading.Thread ListenThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartListening));
                    ListenThread.Start();

                    System.Threading.Thread ListenNewDeviceThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartNewServiceListening));// Listerservice for new devices VF 1000      )
                    ListenNewDeviceThread.Start();

                    System.Threading.Thread ListenUDPThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartUDPService)); // UDP service listen
                    ListenUDPThread.Start();// UDP service listen

                    System.Threading.Thread ListenUDPNewThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartNewUDPService)); // UDP new device service listen
                    ListenUDPNewThread.Start();//new UDP service listen

                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Service started at " + IPAddress + "," + CMDPort);
                    //for (; ; ) { System.Threading.Thread.Sleep(1000 * 60 * 5); } // comment for Service
                }
                catch (Exception exp)
                {
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Failed to start HB service at " + IPAddress + "," + HBport + ", Reason " + exp.Message);
                }
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartService Error " + ex.Message.ToString());
            }
        }

        public void StartServiceInPort(string IPaddress, string port, string NewDevicePort)
        {
            try
            {
                TcpServer = new AsyncSocketAcceptor();
                TcpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));
                // new device                
                TcpNewServer = new AsyncSocketAcceptor();
                TcpNewServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory(false, null)));

                if (IsServerRunning)
                {   // Stop listening
                    if (TcpServer != null)
                    {
                        TcpServer.Dispose();
                        TcpServer = null;
                    }
                    IsServerRunning = false;
                }
                else
                {
                    // Bind listening port，Start listening
                    TcpServer.Bind(new IPEndPoint(IPAddress.Parse(IPaddress), int.Parse(port)));
                    TcpNewServer.Bind(new IPEndPoint(IPAddress.Parse(IPaddress), int.Parse(NewDevicePort)));
                    IsServerRunning = true;
                }
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartService( " + ex.Message.ToString() + ")"); }
        }

        public void StartUDPServiceInPort(string ServerIP, string HBport, string NewDeviceHBPort)
        {
            try
            {
                UdpServer = new AsyncDatagramAcceptor();
                UdpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));
                // new device
                UdpNewServer = new AsyncDatagramAcceptor();
                UdpNewServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory(false, null)));
                UdpNewServer.SessionConfig.ReuseAddress = true;

                // bind listener with address and port
                UdpServer.Bind(new IPEndPoint(IPAddress.Parse(ServerIP), int.Parse(HBport)));
                UdpNewServer.Bind(new IPEndPoint(IPAddress.Parse(ServerIP), int.Parse(NewDeviceHBPort)));
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartUDPServiceInPort( " + ex.Message.ToString() + ")"); }
        }

        #region NORMAL FACE READER COMMUNICATION
        public void StartListening()
        {
            try
            {
                // Open listening                
                //TcpServer.SessionOpened += (o, ea) =>
                //{
                //    if (!checkBoxPassiveEncryption.Checked)
                //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
                //};
                TcpServer.MessageReceived += OnMessageReceived;
                TcpServer.ExceptionCaught += (o, ea) =>
                {   // Thread safety
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartListening(exception  " + ea.Exception.Message.ToString() + ")");
                    ea.Session.Close(true);
                };
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartListening( General Exception ( " + ex.Message.ToString() + ")");
            }
        }
        private void OnMessageReceived(object sender, IoSessionMessageEventArgs ea)
        {
            string DeviceSN = string.Empty;
            AttendanceManager AttnM = new AttendanceManager();  // create the object of attendance manager for attendance data insert
            JobManager JobM = new JobManager();  // create the instance of the job manager            
            Comm cm = new Comm();

            try
            {
                // Open listening                
                //TcpServer.SessionOpened += (o, ea) =>
                //{
                //    if (!checkBoxPassiveEncryption.Checked)
                //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
                //};
                // Display the received string
                //while (true)
                //{
                string Message = (string)ea.Message;
                //       Console.WriteLine(Message);
                if (Message.StartsWith("PostRecord"))
                {   // Extract the serial number and save
                    string SerialNumber = cm.GetDataFromKey(Message, "sn");
                    ea.Session.SetAttribute(KEY_SERIALNUMBER, SerialNumber);
                    DeviceSN = cm.GetDataFromKey(Message, "sn");
                    // Reply is ready to receive attendance records
                    // Need to upload photos
                    ea.Session.Write("Return(result=\"success\" postphoto=\"true\")");

                }
                else if (Message.StartsWith("Record"))
                {   // Get device serial number
                    DeviceSN = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);
                    // Processing card point data
                    if (DeviceSN.Length < 16)
                        DeviceSN = DeviceSN.PadLeft(16, '0');
                    else if (DeviceSN.Length > 16)
                        DeviceSN = DeviceSN.Substring(DeviceSN.Length - 16);
                    // implemented from the version 1.2.1 for direct insert to db ////////////
                    Attendance Attn = new Attendance();
                    Attn.CPUID = DeviceSN;
                    Attn.Command = 1001;
                    Attn.CommandNo = 0;
                    Attn.CommandData = Message;
                    Attn.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                    Attn.SendTo = 'S';
                    if (!AttnM.Insert(Attn)) // tblTAttendanceJob failed
                    {
                        // failed to insert into attednance file.log the data in some text file
                        //so same data will come again. So no need to write to DNP file
                        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed Attn Insert to tblTAttendanceJob " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + ")");
                    }
                    else // tblTAttendanceJob table success
                    {
                        try
                        {
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message); // write to raw file
                            // Server response
                            ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                        }
                        catch (Exception ex)
                        {
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                        }
                    }
                }
                else if (Message.StartsWith("PostEmployee"))
                {   // Prepare to upload personnel
                    ea.Session.Write("Return(result=\"success\")");
                    DeviceSN = cm.GetDataFromKey(Message, "sn");
                    ea.Session.SetAttribute(KEY_SERIALNUMBER, DeviceSN);  // set attribute to get employee template
                }
                else if (Message.StartsWith("Employee"))
                {   // Processing personnel data
                    DeviceSN = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);
                    Job job = new Job();
                    job.CPUID = DeviceSN;
                    job.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                    job.Command = Comm.GetJobCommandFromEnum(EnumJobCommand.FaceTemplate);//face template job
                    if (JobM.ProcessJobReply(job, Message.ToString()))    // send the job for process the reply
                    {
                        // Server response
                        ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                    }
                    else
                        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->ProcessJobReply-Employee, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                }
                else if (Message.StartsWith("GetRequest"))
                {   // Issue an order
                    DeviceSN = cm.GetDataFromKey(Message, "sn");
                    string SerialNumber = cm.GetDataFromKey(Message, "sn");
                    ea.Session.SetAttribute(KEY_SERIALNUMBER, SerialNumber);

                    Job oJob = JobM.GetNextJobsForThisCPUID(SerialNumber);
                    if (string.IsNullOrEmpty(oJob.RefNo) == true)// no job present 
                    { // no job found then device info command                        
                        oJob = JobM.GetDeviceInformationCommand(SerialNumber);// load the get device information command to get the device status information  
                    }

                    var vJob = MasterListOfJobs.Find(c => c.CPUID.Contains(SerialNumber));
                    if (vJob != null) // old job present in the master list // overwrite it
                    {
                        vJob.JobID = oJob.JobID;
                        vJob.RefNo = oJob.RefNo;
                        vJob.Command = oJob.Command;
                        vJob.CommandData = oJob.CommandData;
                        vJob.ThisSessionJobNo = oJob.ThisSessionJobNo;// first jobvJob.ThisSessionJobNo = 1;// first job    
                    }
                    else
                        MasterListOfJobs.Add(oJob);
                    ea.Session.Write(oJob.CommandData); // // date time set job sent to sevice first job                    
                    if (oJob.Command != "08" && oJob.Command != "03")
                        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + SerialNumber + ".log", SerialNumber + ", Get Request From CPUID - " + SerialNumber + ", Job Ref No " + oJob.RefNo + ", Command " + (oJob.CommandData.Length > 25 ? oJob.CommandData.Substring(0, 25) : oJob.CommandData));
                }
                else if (Message.StartsWith("Return"))
                {
                    string SerialNumber = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);
                    var vJob = MasterListOfJobs.Find(c => c.CPUID.Contains(SerialNumber));
                    if (vJob != null)
                    {
                        if (vJob.Command != "08" && vJob.Command != "03")
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + SerialNumber + ".log", SerialNumber + ",Return Job Ref No " + vJob.RefNo + ", Command " + (vJob.CommandData.Length > 25 ? vJob.CommandData.Substring(0, 25) : vJob.CommandData) + ",Reply " + Message);
                        if (JobM.ProcessJobReply(vJob, Message.ToString()) == false)    // send the job for process the reply
                        {
                            ea.Session.Close(true);  // send the end of job to the device 
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + SerialNumber + ".log", "Failed TO Finished Job " + SerialNumber + " " + vJob.RefNo);
                        }
                        else  // go for next job
                        {
                            if (vJob.ThisSessionJobNo <= conNoOfOldJobsToBeSentInOneSession) // the no of jobs sent < no of jobs to be sent
                            {
                                Job oJob = new Job(); // new job object
                                if (vJob.Command == "08") // device info command executed then go for date time set command
                                {
                                    oJob = JobM.GetDateTimeSetCommand(SerialNumber); // date time set command to be loaded
                                }
                                else // previous job not a device info job then check job table for new job
                                {
                                    oJob = JobM.GetNextJobsForThisCPUID(SerialNumber); // get next job from the table
                                }
                                if (string.IsNullOrEmpty(oJob.RefNo) == false)// some job present 
                                {
                                    vJob.JobID = oJob.JobID;
                                    vJob.RefNo = oJob.RefNo;
                                    vJob.Command = oJob.Command;
                                    vJob.CommandData = oJob.CommandData;
                                    vJob.ThisSessionJobNo = (short)(vJob.ThisSessionJobNo + 1); // increase the job count passed to device    
                                    ea.Session.Write(oJob.CommandData); // date time set job sent to sevice first job     
                                }
                                else
                                {
                                    ea.Session.Close(true);// send the end of job to the device // close the session}
                                }
                            }
                            else // no of sent to the device  > no of jobs to be sent
                            { // no more jobs to be sent to the device
                                ea.Session.Close(true);  // send the end of job to the device // close the session
                            }
                        }
                    }
                    else
                        ea.Session.Close(true);  // send the end of job to the device // close the session
                }
                else if (Message.StartsWith("Quit"))
                {   // Disconnect
                    ea.Session.Close(true);
                    //break;
                }

                //TcpServer.ExceptionCaught += (o, ea) =>
                //{   // Thread safety
                //        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "exception , SN- " + DeviceSN + " " + ea.Exception.Message.ToString());
                //    ea.Session.Close(true);
                //};
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", ex.Message.ToString());

            }
        }

        /////////////// UDP service event//////////////////////////
        private void StartUDPService()
        {
            try
            {
                UdpServer.SessionConfig.ReuseAddress = true;
                UdpServer.MessageReceived += (s, ea) =>
                {
                    string Message = (string)ea.Message;
                    // ea.Session.Write("PostRequest()");
                };

                UdpServer.ExceptionCaught += (s, ea) =>
                {
                    //Console.WriteLine("Exception: " + ea.Exception.Message.ToString() + "\r\n");
                    ea.Session.Close(true);
                };
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartUDPService(" + ex.Message.ToString() + ")"); }
        }
        #endregion NORMAL FACE READER COMMUNICATION

        #region NEW FACE READERS JSON COMUNICATION VF1000

        private void StartNewServiceListening()
        {
            //TcpNewServer.SessionOpened += (o, ea) =>
            //{
            //    if (!checkBoxPassiveEncryption.Checked)
            //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
            //};            
            try
            {
                TcpNewServer.MessageReceived += OnNewMessageReceived;
                TcpNewServer.ExceptionCaught += (o, ea) =>
                {   // Thread safety
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening(exception , " + ea.Exception.Message.ToString());
                    ea.Session.Close(true);
                };
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening(General Exception " + ex.Message.ToString() + ")");
            }
        }

        private void OnNewMessageReceived(object sender, IoSessionMessageEventArgs ea)
        {
            string DeviceSN = string.Empty;
            bool IsSuccess = false;
            AttendanceManager AttnM = new AttendanceManager();  // create the object of attendance manager for attendance data insert
            JobManager JobM = new JobManager();  // create the instance of the job manager            
            Comm cm = new Comm();
            try
            {
                string Message = (string)ea.Message;
                //Console.WriteLine(Message);
                //cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", Message);  // write received data
                COMMAND_PCIR_TYPE A = Message.FromJsonTo<COMMAND_PCIR_TYPE>();
                if (string.Equals(A.COMMAND, "RecogniseResult"))                 // Attendance data and Post Employeee data
                {
                    IsSuccess = false;  //  default value
                    if (Message.IndexOf("face_data") < 0) // Attendance data , other wise post employee data
                    {
                        Attendance Attn = new AttendanceManager().ProcessAttendanceDataFromJson(Message);//convert json string into attendance data
                        if (strInsertPostPhotoToDB.ToUpper() == "NO")// face photo will not be inserted
                        {// get the capture photo from the json string and then replace with blank string in command data
                            string captureJpg = cm.GetDataFromJsonStringUsingKey(Message, "capturejpg");// get the capture jpg data
                            Attn.CommandData = Message.Replace(captureJpg, "");// replace the command data without capture face photo
                        }
                        if (!AttnM.Insert(Attn)) // tblTAttendanceJob failed
                        {
                            // failed to insert into attednance file.log the data in some text file
                            //so same data will come again. So no need to write to DNP file
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnNewMessageReceived(Error- Failed Attn Insert to tblTAttendanceJob " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + ")");
                        }
                        else // tblTAttendanceJob table success
                        {
                            DeviceSN = Attn.CPUID;// assign the CPU ID retrieved from the JSON string  
                            try
                            {
                                string captureJpg = cm.GetDataFromJsonStringUsingKey(Message, "capturejpg");// get the capture jpg data
                                /////// replace the capture jpg data with blank for raw file
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message.Replace(captureJpg, "")); // write to raw file
                                IsSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnNewMessageReceived(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                            }
                        }
                    }
                    else  // post employee record, insert to job table
                    {
                        ReceivedCommandData RCD = new Json().ConvertJSonToObject<ReceivedCommandData>(Message);// convert Json to class object
                        Job job = new Job();
                        job.CPUID = RCD.PARAM.sn;  // CPU ID
                        job.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                        job.Command = "V2200"; // Comm.GetJobCommandFromEnum(EnumJobCommand.FaceTemplate);//face template job
                        job.CommandNo = 0;
                        job.CommandData = Message;
                        job.SendTo = 'S';
                        if (strInsertFaceDataForNewDevice.ToUpper() == "YES")  // update tblJob table with face template data
                        {
                            if (JobM.Insert(job))    //post employee record inserted  to tblTjob success                                                 
                                IsSuccess = true;   // Reply data received successfully                        
                            else
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening->Post employee record, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                        }
                        IsSuccess = true;
                    }
                    if (IsSuccess == true) //// Server response success
                    {
                        try
                        {
                            REPLY_PCIR_TYPE M = new REPLY_PCIR_TYPE();
                            M.RETURN = "RecogniseResult";
                            M.PARAM = new PARAM_REPLY_PCIR_TYPE();
                            M.PARAM.result = "success";
                            ea.Session.Write(M.ToJsonString()); ;   //  data received successfully
                        }
                        catch (Exception ex)
                        {
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnNewMessageReceived(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                        }
                    }
                }
                else if (string.Equals(A.COMMAND, "GetRequest"))//  request for job from device
                {
                    ReceivedCommandData RCD = new Json().ConvertJSonToObject<ReceivedCommandData>(Message);// convert Json to class object
                    DeviceSN = RCD.PARAM.sn;// get device serial no                
                    ea.Session.SetAttribute(KEY_SERIALNUMBER, DeviceSN);  // set attribute according to recomendation 
                    Job oJob = JobM.GetNextJobsForThisCPUID(DeviceSN);
                    if (string.IsNullOrEmpty(oJob.RefNo) == true)// no job present 
                    { // no job found then device info command
                      // Console.WriteLine("Device Info For" + DeviceSN);
                        oJob = JobM.GetNewDeviceInformationCommand(DeviceSN);// load the get device information command to get the device status information  
                    }

                    var vJob = MasterListOfJobs.Find(c => c.CPUID.Contains(DeviceSN));
                    if (vJob != null) // old job present in the master list // overwrite it
                    {
                        vJob.JobID = oJob.JobID;
                        vJob.RefNo = oJob.RefNo;
                        vJob.Command = oJob.Command;
                        vJob.CommandData = oJob.CommandData;
                        vJob.ThisSessionJobNo = 1;// first job in this session
                    }
                    else
                        MasterListOfJobs.Add(oJob);
                    ea.Session.Write(oJob.CommandData); //send the job to the device
                    if (oJob.Command != "08" && oJob.Command != "03")
                        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".log", DeviceSN + ", Get Request From CPUID - " + DeviceSN + ", Job Ref No " + oJob.RefNo + ", Command " + (oJob.CommandData.Length > 25 ? oJob.CommandData.Substring(0, 25) : oJob.CommandData));
                }
                else if (string.Equals(A.COMMAND, "Return"))
                {
                    DeviceSN = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);
                    ReceivedCommandData RCD = new Json().ConvertJSonToObject<ReceivedCommandData>(Message);// convert Json to class object               
                    var vJob = MasterListOfJobs.Find(c => c.CPUID.Contains(DeviceSN));
                    if (vJob != null)
                    {
                        if (vJob.Command != "08" && vJob.Command != "03")  // not the device info command 
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".log", DeviceSN + ",Return Job Ref No " + vJob.RefNo + ", Command " + (vJob.CommandData.Length > 25 ? vJob.CommandData.Substring(0, 25) : vJob.CommandData) + ",Reply " + Message);
                        if (JobM.ProcessNewDeviceJobReply(vJob, Message.ToString()) == false)    // send the job for process the reply
                        {
                            ea.Session.Close(true);  // send the end of job to the device 
                                                     //Console.WriteLine("Failed TO Finished Job " + DeviceSN + " " + vJob.RefNo);
                        }
                        else// go for next job
                        {
                            if (vJob.ThisSessionJobNo <= conNoOfNewJobsToBeSentInOneSession) // the no of jobs sent < no of jobs to be sent
                            {
                                Job oJob = new Job(); // new job object
                                if (vJob.Command == "08") // device info command executed then go for date time set command
                                {
                                    oJob = JobM.GetNewDeviceDateTimeSetCommand(DeviceSN); // date time set command to be loaded
                                }
                                else // previous job not a device info job then check job table for new job
                                {
                                    oJob = JobM.GetNextJobsForThisCPUID(DeviceSN); // get next job from the table
                                }
                                if (string.IsNullOrEmpty(oJob.RefNo) == false)// some job present 
                                {
                                    vJob.JobID = oJob.JobID;
                                    vJob.RefNo = oJob.RefNo;
                                    vJob.Command = oJob.Command;
                                    vJob.CommandData = oJob.CommandData;
                                    vJob.ThisSessionJobNo = (short)(vJob.ThisSessionJobNo + 1); // increase the job count passed to device    
                                    ea.Session.Write(oJob.CommandData); // date time set job sent to sevice first job     
                                }
                                else // no job found
                                {
                                    ea.Session.Close(true);// send the end of job to the device // close the session}
                                }
                            }
                            else // no of sent to the device  > no of jobs to be sent
                            { // no more jobs to be sent to the device
                                ea.Session.Close(true);  // send the end of job to the device // close the session
                            }
                        }
                    }
                    else  // could not found job in the master list then close the session
                        ea.Session.Close(true);  // send the end of job to the device // close the session
                }
                else // no command to process then close the session
                { ea.Session.Close(true); }
            }
            catch (Exception ex) { cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnNewMessageReceived(General Exception " + ex.Message + ")"); }
        }

        // New UDP Service
        private void StartNewUDPService()
        {
            try
            {
                UdpNewServer.MessageReceived += (s, ea) =>
                {
                    string Message = (string)ea.Message;
                    /*{
                        "RETURN":"DevStatus",
                        "PARAM":{
                                                        "result":"success/fail",
                            "reason":"",
                            "command":"PostRequest"

                        }
                    }*/
                    //ea.Session.Write("PostRequest()");
                    REPLY_HEARTBEAT_TYPE rht = new REPLY_HEARTBEAT_TYPE();
                    rht.PARAM = new PARAM_REPLY_HEARTBEAT_TYPE();
                    //ea.Session.Write(JsonHelper.ToJsonString(rht));
                    //Console.WriteLine("Send: " + JsonHelper.ToJsonString(rht));
                };

                UdpNewServer.ExceptionCaught += (s, ea) =>
                {
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Exception: " + ea.Exception.Message.ToString() + ")");
                    ea.Session.Close(true);
                };
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewUDPService( Exception " + ex.Message + ")"); }
        }

        #endregion NEW FACE READERS JSON COMUNICATION VF1000
    }
}
