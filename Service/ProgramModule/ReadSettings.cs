﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PurpleMoorFDService.ProgramModule
{
    public partial class ReadSettings
    {
        public static string getConnectionString()
        {

            string dbPath = System.Configuration.ConfigurationManager.AppSettings["DatabasePath"];
            return dbPath;
        }

        /// <summary>
        /// get server port to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerCMDPort()
        {

            string serverPort = "9902";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerCMDPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerCMDPort"];
            return serverPort;
        }

        /// <summary>
        /// get server port to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerHBPort()
        {

            string serverPort = "9904";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerHBPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerHBPort"];
            return serverPort;
        }
        /// <summary>
        /// get server IP to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerIP()
        {

            string serverIP = "127.0.0.0";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerIP"] != null)
                serverIP = System.Configuration.ConfigurationManager.AppSettings["ServerIP"];
            return serverIP;
        }


        public static string getServerCMDNewPort()
        {
            string serverPort = "9910";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerCMDNewPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerCMDNewPort"];
            return serverPort;
        }
        public static string getServerHBNewPort()
        {

            string serverPort = "9911";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerHBNewPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerHBNewPort"];
            return serverPort;
        }
        public static string getFaceDeviceTemplateInsertSetting()
        {
            string result = "No";
            if (System.Configuration.ConfigurationManager.AppSettings["InsertFaceTemplate"] != null)
                result = System.Configuration.ConfigurationManager.AppSettings["InsertFaceTemplate"];
            return result;
        }
        public static string getFacePhotoInsertSetting()
        { // settings for insert face photo to db table
            string result = "Yes";
            if (System.Configuration.ConfigurationManager.AppSettings["InsertFacePhoto"] != null)
                result = System.Configuration.ConfigurationManager.AppSettings["InsertFacePhoto"];
            return result;
        }
    }
}
