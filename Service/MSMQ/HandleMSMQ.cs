﻿using PurpleMoorFDService.ProgramModule;
using System;
using System.Messaging;
using System.Timers;

namespace PurpleMoorFDService.MSMQ
{
    public class HandleMSMQ
    {
        private string CaptureDataMessageQName;
        private bool ReadingProcessOn = false;
        public HandleMSMQ (string MSMQName)
        {
            CaptureDataMessageQName = MSMQName;
            string msgQName = ".\\Private$\\" + CaptureDataMessageQName;
            MessageQueue msQue = new MessageQueue(msgQName);

            if (!MessageQueue.Exists(msgQName))  // if not present then create the MSMQ
            {
                try
                {
                    MessageQueue.Create(msgQName);
                    msQue.SetPermissions("Everyone", MessageQueueAccessRights.FullControl, AccessControlEntryType.Allow);
                    msQue.SetPermissions("SYSTEM", MessageQueueAccessRights.FullControl, AccessControlEntryType.Allow);
                }
                catch { }
            }

        }
        public Boolean WriteToMessageQ(string capturedata, Boolean Extended = true)
        {
            try
            {
                string msgQName = ".\\Private$\\" + CaptureDataMessageQName;
                MessageQueue msQue = new MessageQueue(msgQName);
                if (MessageQueue.Exists(msgQName))  // if MSMQ exists then write the data
                {
                    msQue.DefaultPropertiesToSend.Recoverable = true;  
                    msQue.Send(capturedata);
                    return true;
                }
                else
                    return false;
            }
            catch (MessageQueueException err)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", err.Message.ToString());
                return false;
            }
        }

        public void ReadFromMessageQ(object source, ElapsedEventArgs e)  // this is to write in the raw file
        {
            if (ReadingProcessOn == false)
            {
                ReadingProcessOn = true;
                string msgQName = ".\\Private$\\" + CaptureDataMessageQName;
                if (MessageQueue.Exists(msgQName))
                {
                    try
                    {                        
                        MessageQueue msQue = new MessageQueue(msgQName);
                        msQue.Formatter = new XmlMessageFormatter(new string[] { "System.String,mscorlib" });
                        for (int i = 0; i < 10; i++)
                        {
                            Message msg = msQue.Receive(new TimeSpan(0, 0, 3));
                            if (msg.Body.ToString().Length > 0)// some data found
                            {
                                string msgData = msg.Body.ToString();
                                string data = msgData.Substring(0, msgData.IndexOf('~'));
                                string SN = msgData.Substring(msgData.IndexOf("~") + 1);
                                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + SN + ".raw", data); // write to raw file
                            }
                        }
                    }
                    catch (MessageQueueException err)
                    {
                        if (err.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)  // other than read time out error
                            new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", err.Message.ToString());
                    }
                    catch (Exception ex)
                    {
                        new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- ReadFromMessageQ,Error- " + ex.Message.ToString());
                    }
                }
                ReadingProcessOn = false;
            }
        }

    } // class ends
}  // namespace
