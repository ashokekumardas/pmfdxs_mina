﻿using System;
using System.Timers;

namespace PMFDXSHealthChecker
{
    public class HealthChecker
    {
        public static Timer timerHealthCheck;
        public static Timer timerCleanFiles;

        public void StartCheking()
        {
            EventLogger EL = new EventLogger();
            timerCleanFiles = new Timer(12 * 60 * 60 * 1000);
            timerCleanFiles.Elapsed += new ElapsedEventHandler(EL.CleanLogFiles);
            timerCleanFiles.Enabled = true;

            timerHealthCheck = new Timer(5 * 60 * 1000);
            timerHealthCheck.Elapsed += new ElapsedEventHandler(ChckNStartService);
            timerHealthCheck.Enabled = true;
        }

        private void ChckNStartService(object obj,ElapsedEventArgs e)
        {
            new EventLogger().WriteToFileThreadSafe("Start Health Checking", true);// service start log
            System.ServiceProcess.ServiceController[] services = System.ServiceProcess.ServiceController.GetDevices();
            for (int i = 0; i < services.Length; i++)// check all the services
            {
                if(services[i].DisplayName.IndexOf("PMFDService_")>=0) // PMFDXS service found
                { 
                    if(services[i].Status==System.ServiceProcess.ServiceControllerStatus.Stopped) // service stopped
                    {
                        try
                        {
                            services[i].Start();  // start the service
                            for (int x = 0; x < 200000; x++) { } //time killer
                            new EventLogger().WriteToFileThreadSafe("Service resumed " + services[i].DisplayName, true);// service start log
                        }
                        catch(Exception ex) // error in starting service
                        { new EventLogger().WriteToFileThreadSafe("Error in starting service " + services[i].DisplayName + " " + ex.Message, false); }
                    }
                }
            }
        }
    }
}
