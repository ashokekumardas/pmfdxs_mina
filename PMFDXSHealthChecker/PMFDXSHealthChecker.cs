﻿using System.ServiceProcess;

namespace PMFDXSHealthChecker
{
    public partial class PMFDXHealthCheckerService : ServiceBase
    {
        public PMFDXHealthCheckerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            new HealthChecker().StartCheking();
        }

        protected override void OnStop()
        {
        }
    }
}
