﻿
namespace PMFDXSHealthChecker
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PMFDXHealthCheckerServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.PMFDXHealthServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // PMFDXHealthCheckerServiceProcessInstaller
            // 
            this.PMFDXHealthCheckerServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.PMFDXHealthCheckerServiceProcessInstaller.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PMFDXHealthServiceInstaller});
            this.PMFDXHealthCheckerServiceProcessInstaller.Password = null;
            this.PMFDXHealthCheckerServiceProcessInstaller.Username = null;
            // 
            // PMFDXHealthServiceInstaller
            // 
            this.PMFDXHealthServiceInstaller.ServiceName = "PMFDXHealthCheckerService";
            this.PMFDXHealthServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PMFDXHealthCheckerServiceProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller PMFDXHealthCheckerServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller PMFDXHealthServiceInstaller;
    }
}