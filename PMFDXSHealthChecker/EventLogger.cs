﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Timers;

namespace PMFDXSHealthChecker
{
    static class GlobalVariables
    {
        public static string ServiceApplicationName = Process.GetCurrentProcess().ProcessName;// application name;
        public static string ServiceApplicationFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        public static string ServiceApplicationLogFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" + ServiceApplicationName + "_Logs";
    }

    public class EventLogger
    {

        private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();

        public void WriteToFileThreadSafe(string RecordToWrite, bool IsInformation)
        {
            CheckNCreateFolder(GlobalVariables.ServiceApplicationLogFolder);// check the log folder
            string FilePathName = GlobalVariables.ServiceApplicationLogFolder + "\\" + GlobalVariables.ServiceApplicationName + "-" + DateTime.Now.ToString("dd-MMM-yyyy") + ".log";
            // Set Status to Locked
            _readWriteLock.EnterWriteLock();
            try
            {
                // Append text to the file
                using (StreamWriter sw = File.AppendText(FilePathName))
                {
                    sw.WriteLine(DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") + " - " + (IsInformation ? "Information" : "Error") + " - " + RecordToWrite);
                    sw.Close();
                }
            }
            catch (Exception ex) { WriteToEventLog(GlobalVariables.ServiceApplicationName, "WriteToLogFile", RecordToWrite + "-" + ex.Message, false); }
            finally
            {
                // Release lock
                _readWriteLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// write the event error in the windows event log
        /// </summary>
        /// <param name="AppName"></param>
        /// <param name="EventName"></param>
        /// <param name="Entry"></param>
        /// <param name="InformationMsg"></param>
        public void WriteToEventLog(string ApplicationName, string EventName, string Description, bool InformationMsg)
        {
            string appName = ApplicationName + " [" + EventName + "]"; // set the appliaction name
            string logName = "Application";
            EventLog objEventLog = new EventLog();
            try
            {
                //  'Register the App as an Event Source
                if (!EventLog.SourceExists(appName))
                    EventLog.CreateEventSource(appName, logName);
                objEventLog.Source = appName;
                if (InformationMsg == true)
                    objEventLog.WriteEntry(Description, EventLogEntryType.Information);
                else
                    objEventLog.WriteEntry(Description, EventLogEntryType.Error);
            }
            catch { }
        }

        public void CheckNCreateFolder(string FolderName)
        {
            try
            {
                if (!Directory.Exists(FolderName))
                    Directory.CreateDirectory(FolderName);
            }
            catch { }
        }

        /// deletes the files which are older than the specified period
        public static void CheckNDeleteOldFiles(string FolderName, string FileName, Int16 OlderThan)
        {
            string[] FileInfos = Directory.GetFiles(FolderName, FileName);
            foreach (string eachfile in FileInfos)
            {
                FileInfo FI;
                try
                {
                    FI = new FileInfo(eachfile);  // get the creation date of the file
                    TimeSpan TS = DateTime.Now.Subtract(FI.CreationTime);  // get the date difference
                    if (TS.Days >= OlderThan)   // check the file older than specified period
                        FI.Delete();// delete the file
                }
                catch { }
            }
        }

        public void CleanLogFiles(object obj, ElapsedEventArgs e)
        {
            string FileName = "*.Log";
            CheckNDeleteOldFiles(GlobalVariables.ServiceApplicationLogFolder, FileName, 15);
        }
    }
}

