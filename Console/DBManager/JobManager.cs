﻿using PMFDServiceDAL;
using PurpleMoorFDService.ProgramModule;
using System;
using System.Collections.Generic;

namespace PurpleMoorFDService.DBManager
{
    public class JobManager
    {
        private string ErrRpt;
        public string GetErrorReport() { return ErrRpt; }
        /// <summary>
        /// Creates and adds Date Time set command to the job list for the device
        /// </summary>
        /// <param name="CPUID"></param>
        /// <param name="ListOfJobs"></param>
        public void LoadDateTimeSetCommandInToList(string CPUID, List<Job> ListOfJobs)
        {
            try
            {
                DateTime DeviceTimeToSet = DateTime.Now;// current server time
                //////////get timezone from the device status table //////////////
                DeviceStatus DS = new DeviceStatusManager().GetOpenDeviceStatusByCPUID(CPUID);
                if (DS.TimeZone.Length > 1)
                {
                    string hhmmss = DS.TimeZone.Substring(1);// get hour minute without +/- sign "+053000"
                    int timezoneinsec = 0;
                    if (hhmmss.Length >= 2)
                        timezoneinsec = Convert.ToInt16(hhmmss.Substring(0, 2)) * 60 * 60;// convert hour to second
                    if (hhmmss.Length >= 4)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(2, 2)) * 60;// convert minute to second
                    if (hhmmss.Length >= 6)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(4, 2));// add second
                    if (DS.TimeZone.Substring(0, 1) == "+" || DS.TimeZone.Substring(0, 1) == "-") // add or substruct sign
                    {
                        TimeSpan TS = new TimeSpan(0, 0, timezoneinsec);
                        if (DS.TimeZone.Substring(0, 1) == "+") // add timezone time in seconds to UTC
                            DeviceTimeToSet = DateTime.UtcNow.Add(TS); // add time to utc get the time zone 
                        else // substruct  second from the utc time
                            DeviceTimeToSet = DateTime.UtcNow.Subtract(TS); // add time to utc get the time zone 
                    }
                    else
                        new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Inner else part - Invalid Timezone " + DS.TimeZone + ")");
                }
                else // invalid timezone
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Invalid Timezone " + DS.TimeZone + ")");
                /////////////////////////////////////////////////////////////////////////////
                Job oJob = new Job();
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "03";
                oJob.CommandData = "SetDeviceInfo(time=\"" + DeviceTimeToSet.ToString("yyyy-MM-dd HH:mm:ss") + "\" week=\"" +
                                    (int)DeviceTimeToSet.DayOfWeek + "\")";
                oJob.Priority = '0';
                ListOfJobs.Add(oJob);
            }
            catch (Exception ex) 
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(" + ex.Message + ")");
            }
        }

        public Job GetDateTimeSetCommand(string CPUID)
        {
            Job oJob = new Job();
            try
            {
                DateTime DeviceTimeToSet = DateTime.Now;// current server time
                //////////get timezone from the device status table //////////////
                DeviceStatus DS = new DeviceStatusManager().GetOpenDeviceStatusByCPUID(CPUID);
                if (DS.TimeZone.Length > 1)
                {
                    string hhmmss = DS.TimeZone.Substring(1);// get hour minute without +/- sign "+053000"
                    int timezoneinsec = 0;
                    if (hhmmss.Length >= 2)
                        timezoneinsec = Convert.ToInt16(hhmmss.Substring(0, 2)) * 60 * 60;// convert hour to second
                    if (hhmmss.Length >= 4)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(2, 2)) * 60;// convert minute to second
                    if (hhmmss.Length >= 6)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(4, 2));// add second
                    if (DS.TimeZone.Substring(0, 1) == "+" || DS.TimeZone.Substring(0, 1) == "-") // add or substruct sign
                    {
                        TimeSpan TS = new TimeSpan(0, 0, timezoneinsec);
                        if (DS.TimeZone.Substring(0, 1) == "+") // add timezone time in seconds to UTC
                            DeviceTimeToSet = DateTime.UtcNow.Add(TS); // add time to utc get the time zone 
                        else // substruct  second from the utc time
                            DeviceTimeToSet = DateTime.UtcNow.Subtract(TS); // add time to utc get the time zone 
                    }
                    else
                        new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetDateTimeSetCommand(Inner else part - Invalid Timezone " + DS.TimeZone + ")");
                }
                else // invalid timezone
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetDateTimeSetCommand(Invalid Timezone " + DS.TimeZone + ")");
                /////////////////////////////////////////////////////////////////////////////
               
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "03";
                oJob.CommandData = "SetDeviceInfo(time=\"" + DeviceTimeToSet.ToString("yyyy-MM-dd HH:mm:ss") + "\" week=\"" +
                                    (int)DeviceTimeToSet.DayOfWeek + "\")";
                oJob.Priority = '0';
                
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetDateTimeSetCommand(" + ex.Message + ")");
            }
            return oJob;
        }

        public bool ProcessJobReply(Job oJob,string ReplyFromDevice)
        {
            bool success=true;
            oJob.ErrCode = string.Empty;// refresh the errcode field
            try
            {
                switch (oJob.Command)
                {
                    case "03":  // date time set command reply nothing to do in the job table
                        break;
                    case "08":  // device info command , update device configuration in the device status table
                        {
                            DeviceStatusManager DSM = new DeviceStatusManager();
                            if (DSM.GetOpenDeviceStatusByCPUID(oJob.CPUID).CPUID == string.Empty)  //no value returned
                                DSM.InsertDeviceStatus(ReplyFromDevice,false);  // insert the record
                            else
                                DSM.UpdateDeviceStatus(ReplyFromDevice,false);  // update the device status record
                            break;
                        }
                    
                    case "F3100" : // capture head photo for valid and invalid photo
                    case "F2200":  // face template return job
                        {
                            Job InsertJob = new Job();
                            InsertJob.CPUID = oJob.CPUID;
                            InsertJob.RefNo = oJob.RefNo;
                            InsertJob.CommandNo = 0;
                            InsertJob.Command = oJob.Command;
                            InsertJob.CommandData = ReplyFromDevice;
                            InsertJob.SendTo = 'S';
                            if (!Insert(InsertJob))
                            {                             //write some error log
                                success = false;
                            }
                            oJob.FinishedOn = DateTime.UtcNow;
                            if (ReplyFromDevice.IndexOf("success") < 0)  // failed command
                            {
                                oJob.ErrCode = "Fail";
                                //new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.ProcessJobReply(Command Failed For " +
                                 //                     oJob.CPUID+" Ref No " +oJob.RefNo + " Reason " + ReplyFromDevice + ")");
                            }
                            UpdateFinishedJob(oJob);  // close the job
                            break;
                        }
                    default:
                        {
                            oJob.FinishedOn = DateTime.UtcNow;
                            if (ReplyFromDevice.IndexOf("success") < 0)  // failed command
                            {
                                oJob.ErrCode = "Fail";
                                //new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.ProcessJobReply(Command Failed For " +
                               //                       oJob.CPUID + " Ref No " + oJob.RefNo + " Reason " + ReplyFromDevice + ")");
                            }
                            else
                            {
                                if (ReplyFromDevice.IndexOf("result=\"success\")") < 0) // return has not data body insert the data as return value
                                {
                                    Job InsertJob = new Job();
                                    InsertJob.CPUID = oJob.CPUID;
                                    InsertJob.RefNo = oJob.RefNo;
                                    InsertJob.CommandNo = 0;
                                    InsertJob.Command = oJob.Command;
                                    InsertJob.CommandData = ReplyFromDevice;
                                    InsertJob.SendTo = 'S';
                                    if (!Insert(InsertJob))
                                    {                             //write some error log
                                        success = false;
                                    }
                                }
                                else
                                {
                                    // only close the job
                                }
                            }
                            UpdateFinishedJob(oJob);  // close the job
                            break;
                        }
                }
            }
            catch (Exception ex) { success = false; ErrRpt = ex.Message; }
            return success;
        }

        /// <summary>
        /// Create and adds to the job list to get device information
        /// </summary>
        /// <param name="CPUID"></param>
        /// <param name="ListOfJobs"></param>
        public void LoadGetDeviceInformationCommandInToList(string CPUID, List<Job> ListOfJobs)
        {
            try
            {
                Job oJob = new Job();
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "08";
                oJob.CommandData = "GetDeviceInfo()";
                oJob.Priority = '0';
                ListOfJobs.Add(oJob);
            }
            catch (Exception) { }
        }

        public Job GetDeviceInformationCommand(string CPUID)
        {
            Job oJob = new Job();
            try
            {
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "08";
                oJob.CommandData = "GetDeviceInfo()";
                oJob.Priority = '0';
            }
            catch (Exception) { }
            return oJob;
        }
        public bool GetNext25JobsForThisCPUID(string CPUID, List<Job> ListOfJob)
        {
            bool success;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                success = new PMFDServiceDAL.CommonDBLayer().GetDeviceJobByCPUID(strDatabaseType, ConnStr, CPUID, ListOfJob);
            }
            catch (Exception ex) // failed 
            {
                ErrRpt = ex.Message.ToString();
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetNext25JobsForThisCPUID( " + ex.Message + ")");
                success = false;
            }
            finally { }
            return success;
        }

        public bool GetNewJobsForThisCPUID(string CPUID, List<Job> ListOfJob,int JobCount)
        {
            bool success;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                success = new PMFDServiceDAL.CommonDBLayer().GetNewDeviceJobByCPUID(strDatabaseType, ConnStr, CPUID, ListOfJob,JobCount);
            }
            catch (Exception ex) // failed 
            {
                ErrRpt = ex.Message.ToString();
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetNewJobsForThisCPUID( " + ex.Message + ")");
                success = false;
            }
            finally { }
            return success;
        }
        public Job GetNextJobsForThisCPUID(string CPUID)
        {
            List<Job> ListOfJob = new List<Job>();
            Job oJob = new Job();
            bool success = true;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                success = new PMFDServiceDAL.CommonDBLayer().GetNewDeviceJobByCPUID(strDatabaseType, ConnStr, CPUID, ListOfJob, 1);
            }
            catch (Exception ex) // failed 
            {
                ErrRpt = ex.Message.ToString();
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.GetNewJobsForThisCPUID( " + ex.Message + ")");
                success = false;
            }
            finally { }
            if(success) // job returned
            {
                if(ListOfJob.Count>0)
                oJob = ListOfJob[0];// get first job
            }
            return oJob;
        }
        public Boolean Insert(Job job)
        {
            bool success = false;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                success = new PMFDServiceDAL.CommonDBLayer().InsertDeviceJob(strDatabaseType, ConnStr, job);
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.Insert( " + ex.Message + " )");
                throw (new Exception(ex.Message.ToString()));
            }
            return success;
        }

        ///// <summary>
        ///// closes the job 
        ///// </summary>
        ///// <param name="oJob"></param>
        ///// <returns></returns>
        public bool UpdateFinishedJob(Job oJob)
        {
            bool success = false;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                success = new PMFDServiceDAL.CommonDBLayer().UpdateDeviceJob(strDatabaseType, ConnStr, oJob);
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message.ToString()));
            }
            return success;
        }

        ///// <summary>
        ///// Inserts temlates to the job table for processing
        ///// </summary>
        ///// <param name="CaptureString"></param>
        ///// <param name="SN"></param>
        ///// <returns></returns>
        //public bool WriteTemplateToJobTable( string CaptureString, string SN)
        //{
        //    string FaceTemplate = string.Empty;
        //    try
        //    {
        //        string ConnStr = SqlDataHelper.GetConnectionString();
        //        using (SqlConnection sqlconn = new SqlConnection(ConnStr))
        //        {
        //            sqlconn.Open();
        //            if (sqlconn.State == ConnectionState.Open)
        //            {
        //                SqlCommand sqlcmd = new SqlCommand();
        //                sqlcmd.Connection = sqlconn;
        //                sqlcmd.CommandText = "Insert into tblTJob (JobID,RefNo,CPUID,CommandNo,Command,CommandData,SendTo) values (" +
        //                               "'" + Guid.NewGuid().ToString() + "','" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "','" + SN + "',0,'3000','" + CaptureString + "','S')";
        //                sqlcmd.ExecuteNonQuery();
        //            }
        //            return true;
        //        }
        //    }
        //    catch (Exception ex) // failed to write into job table
        //    {
        //       // new clsCommon().WriteToWinEventLog(ex.Message.ToString(), 6);
        //        return false;
        //    }
        //}
        #region NEW DEVICE JSON COMMAND DEVICE LIKE VF1000
        public void LoadGetNewDeviceInformationCommandInToList(string CPUID, List<Job> ListOfJobs)
        {
            try
            {
                Job oJob = new Job();
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "08";
                oJob.CommandData = "{\"RETURN\":\"GetRequest\"," +
                                     "\"PARAM\":{\"result\":\"success\"," +
                                     "\"reason\":\"Reason\"," +
                                     "\"command\":\"GetDeviceInfo\"}} ";// GetDeviceInfo()";
                oJob.Priority = '0';
                ListOfJobs.Add(oJob);
            }
            catch (Exception) { }
        }
        public Job GetNewDeviceInformationCommand(string CPUID)
        {
            Job oJob = new Job();
            try
            {                
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "08";
                oJob.CommandData = "{\"RETURN\":\"GetRequest\"," +
                                     "\"PARAM\":{\"result\":\"success\"," +
                                     "\"reason\":\"Reason\"," +
                                     "\"command\":\"GetDeviceInfo\"}} ";// GetDeviceInfo()";
                oJob.Priority = '0';               
            }
            catch (Exception) { }
            return oJob;
        }
        public void LoadNewDeviceDateTimeSetCommandInToList(string CPUID, List<Job> ListOfJobs)
        {
            try
            {
                DateTime DeviceTimeToSet = DateTime.Now;// current server time
                //////////get timezone from the device status table //////////////
                DeviceStatus DS = new DeviceStatusManager().GetOpenDeviceStatusByCPUID(CPUID);
                if (DS.TimeZone.Length > 1)
                {
                    string hhmmss = DS.TimeZone.Substring(1);// get hour minute without +/- sign "+053000"
                    int timezoneinsec = 0;
                    if (hhmmss.Length >= 2)
                        timezoneinsec = Convert.ToInt16(hhmmss.Substring(0, 2)) * 60 * 60;// convert hour to second
                    if (hhmmss.Length >= 4)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(2, 2)) * 60;// convert minute to second
                    if (hhmmss.Length >= 6)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(4, 2));// add second
                    if (DS.TimeZone.Substring(0, 1) == "+" || DS.TimeZone.Substring(0, 1) == "-") // add or substruct sign
                    {
                        TimeSpan TS = new TimeSpan(0, 0, timezoneinsec);
                        if (DS.TimeZone.Substring(0, 1) == "+") // add timezone time in seconds to UTC
                            DeviceTimeToSet = DateTime.UtcNow.Add(TS); // add time to utc get the time zone 
                        else // substruct  second from the utc time
                            DeviceTimeToSet = DateTime.UtcNow.Subtract(TS); // add time to utc get the time zone 
                    }
                    else
                        new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Inner else part - Invalid Timezone " + DS.TimeZone + ")");
                }
                else // invalid timezone
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Invalid Timezone " + DS.TimeZone + ")");
                /////////////////////////////////////////////////////////////////////////////
                Job oJob = new Job();
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "03";
                oJob.CommandData = "{\"RETURN\":\"GetRequest\",\"PARAM\":{\"result\":\"success\",\"reason\":\"Reason\",\"command\":\"SyncTime\"," +
                                   "\"time\":\"" + DeviceTimeToSet.ToString("yyyy-MM-dd HH:mm:ss") + "\"}}";             
                oJob.Priority = '0';
                ListOfJobs.Add(oJob);
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadNewDeviceDateTimeSetCommandInToList(" + ex.Message + ")");
            }
        }

        public Job GetNewDeviceDateTimeSetCommand(string CPUID)
        {
            Job oJob = new Job();
            try
            {
                DateTime DeviceTimeToSet = DateTime.Now;// current server time
                //////////get timezone from the device status table //////////////
                DeviceStatus DS = new DeviceStatusManager().GetOpenDeviceStatusByCPUID(CPUID);
                if (DS.TimeZone.Length > 1)
                {
                    string hhmmss = DS.TimeZone.Substring(1);// get hour minute without +/- sign "+053000"
                    int timezoneinsec = 0;
                    if (hhmmss.Length >= 2)
                        timezoneinsec = Convert.ToInt16(hhmmss.Substring(0, 2)) * 60 * 60;// convert hour to second
                    if (hhmmss.Length >= 4)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(2, 2)) * 60;// convert minute to second
                    if (hhmmss.Length >= 6)
                        timezoneinsec += Convert.ToInt16(hhmmss.Substring(4, 2));// add second
                    if (DS.TimeZone.Substring(0, 1) == "+" || DS.TimeZone.Substring(0, 1) == "-") // add or substruct sign
                    {
                        TimeSpan TS = new TimeSpan(0, 0, timezoneinsec);
                        if (DS.TimeZone.Substring(0, 1) == "+") // add timezone time in seconds to UTC
                            DeviceTimeToSet = DateTime.UtcNow.Add(TS); // add time to utc get the time zone 
                        else // substruct  second from the utc time
                            DeviceTimeToSet = DateTime.UtcNow.Subtract(TS); // add time to utc get the time zone 
                    }
                    else
                        new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Inner else part - Invalid Timezone " + DS.TimeZone + ")");
                }
                else // invalid timezone
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadDateTimeSetCommandInToList(Invalid Timezone " + DS.TimeZone + ")");
                /////////////////////////////////////////////////////////////////////////////
                oJob.RefNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                oJob.CPUID = CPUID;
                oJob.CommandNo = 0;
                oJob.Command = "03";
                oJob.CommandData = "{\"RETURN\":\"GetRequest\",\"PARAM\":{\"result\":\"success\",\"reason\":\"Reason\",\"command\":\"SyncTime\"," +
                                   "\"time\":\"" + DeviceTimeToSet.ToString("yyyy-MM-dd HH:mm:ss") + "\"}}";
                oJob.Priority = '0';                
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.LoadNewDeviceDateTimeSetCommandInToList(" + ex.Message + ")");
            }
            return oJob;
        }

        #endregion NEW DEVICE JSON COMMAND DEVICE LIKE VF1000

        #region NEW DEVICE PROCESS JOB REPLY DEVICE LIKE VF1000
        public bool ProcessNewDeviceJobReply(Job oJob, string ReplyFromDevice)
        {
            bool success = true;
            oJob.ErrCode = string.Empty; // refresh the err code property
            try
            {
                switch (oJob.Command)
                {
                    case "03":  // date time set command reply nothing to do in the job table
                        break;
                    case "08":  // device info command , update device configuration in the device status table
                        {
                            DeviceStatusManager DSM = new DeviceStatusManager();
                            if (DSM.GetOpenDeviceStatusByCPUID(oJob.CPUID).CPUID == string.Empty)  //no value returned
                                DSM.InsertDeviceStatus(ReplyFromDevice,true);  // insert the record
                            else
                                DSM.UpdateDeviceStatus(ReplyFromDevice,true);  // update the device status record
                            break;
                        }

                    case "F3100": // capture head photo for valid and invalid photo
                    case "F2200":  // face template return job
                        {
                            Job InsertJob = new Job();
                            InsertJob.CPUID = oJob.CPUID;
                            InsertJob.RefNo = oJob.RefNo;
                            InsertJob.CommandNo = 0;
                            InsertJob.Command = oJob.Command;
                            InsertJob.CommandData = ReplyFromDevice;
                            InsertJob.SendTo = 'S';
                            if (!Insert(InsertJob))
                            {                             //write some error log
                                success = false;
                            }
                            oJob.FinishedOn = DateTime.UtcNow;
                            if (ReplyFromDevice.IndexOf("success") < 0)  // failed command
                            {
                                oJob.ErrCode = "Fail";
                                //new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.ProcessNewDeviceJobReply(Command Failed For " +
                                 //                     oJob.CPUID + " Ref No " + oJob.RefNo + " Reason " + ReplyFromDevice + ")");
                            }
                            UpdateFinishedJob(oJob);  // close the job
                            break;
                        }
                    default:
                        {
                            oJob.FinishedOn = DateTime.UtcNow;
                            if (ReplyFromDevice.IndexOf("success") < 0) // failed command                           
                            {
                                oJob.ErrCode = "Fail";
                                //new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "JobManager.ProcessNewDeviceJobReply(Command Failed For " +
                                //                      oJob.CPUID + " Ref No " + oJob.RefNo + " Reason " + ReplyFromDevice + ")");
                            }
                            else
                            {
                                if (ReplyFromDevice.IndexOf("\"result\" : \"success\" } }") < 0) // return has data body insert the data as return value
                                {//{ "COMMAND" : "Return", "PARAM" : { "reason" : "", "result" : "success" } }
                                    Job InsertJob = new Job();
                                    InsertJob.CPUID = oJob.CPUID;
                                    InsertJob.RefNo = oJob.RefNo;
                                    InsertJob.CommandNo = 0;
                                    InsertJob.Command = oJob.Command;
                                    InsertJob.CommandData = ReplyFromDevice;
                                    InsertJob.SendTo = 'S';
                                    if (!Insert(InsertJob))
                                    {                             //write some error log
                                        success = false;
                                    }
                                }
                                else  // no data body 
                                {
                                    // only close the job
                                }
                            }
                            UpdateFinishedJob(oJob);  // close the job
                            break;
                        }
                }
            }
            catch (Exception ex) { success = false; ErrRpt = ex.Message; }
            return success;
        }
        #endregion NEW DEVICE PROCESS JOB REPLY DEVICE LIKE VF1000
    }
}
