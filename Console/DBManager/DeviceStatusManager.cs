﻿using System;
using System.Data.SqlClient;
using System.Data;
using PurpleMoorFDService.ProgramModule;
using PMFDServiceDAL;

namespace PurpleMoorFDService.DBManager
{
    public class DeviceStatusManager
    {        

        /// <summary>
        /// Get the open device status for this cpu id
        /// </summary>
        /// <param name="cpuid"></param>
        /// <returns></returns>
        public DeviceStatus GetOpenDeviceStatusByCPUID(string CPUID)
        {
            DeviceStatus result = new DeviceStatus();
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                result = new PMFDServiceDAL.CommonDBLayer().GetDeviceStatusByCPUID(strDatabaseType, ConnStr, CPUID);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return result;
        }

        #region OLD DEVICES LIKE F910, F710 
        private DeviceStatus GetDeviceStatusFromString(string StrDeviceStatus)
        {
            Comm cm = new Comm();
            DeviceStatus DS = new DeviceStatus();
            string IPAddress = cm.GetDataFromKey(StrDeviceStatus, "ip");
            DS.CPUID = cm.GetDataFromKey(StrDeviceStatus, "dev_id");
            DS.DeviceAdd = "00";
            DS.EstbCode = "00000001";
            DS.DeviceType = "FA"; // GetDeviceType(cm.GetDataFromKey(StrDeviceStatus, "type"));
            DS.SubType = cm.GetDataFromKey(StrDeviceStatus, "type");// "FFFFFFFF";
            DS.DeviceStat = cm.GetDataFromKey(StrDeviceStatus, "alg_edition");
            DS.UncapRec = 0;
            DS.EnrolledUser = Int32.Parse(cm.GetDataFromKey(StrDeviceStatus, "real_faceregist"));
            DS.TotalUser  = Int32.Parse(cm.GetDataFromKey(StrDeviceStatus, "real_employee"));
            DS.IPAddress = IPAddress.Substring(0, 3) + "." + IPAddress.Substring(3, 3) + "." + IPAddress.Substring(6, 3) + "." + IPAddress.Substring(9, 3);
            DS.Firmware = cm.GetDataFromKey(StrDeviceStatus, "edition");
            return DS;

        }

        public bool InsertDeviceStatus(string strDeviceStatus,bool IsNewDevice)
        {
            bool success=true;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                DeviceStatus DS = null; // device status object
                if (IsNewDevice == false) // Old devices like F910 , M2000 etc
                    DS = GetDeviceStatusFromString(strDeviceStatus); // convert device status from key value pair , F910
                else
                    DS = GetNewDeviceStatusFromJsonString(strDeviceStatus); // convert Json string to device status VF1000
                new PMFDServiceDAL.CommonDBLayer().InsertDeviceStatus(strDatabaseType, ConnStr,DS);
            }
            catch (Exception ex) // failed 
            {
                //ErrRpt = ex.Message.ToString();
                success = false;
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "DeviceStatusManager.InsertDeviceStatus( " +  ex.Message + ")");
            }
            finally
            {
            }
            return success;

        }
        public bool UpdateDeviceStatus(string  strDeviceStatus,bool IsNewDevice)
        {
            bool success=false;
            try
            {
                string ConnStr = SqlDataHelper.GetConnectionString();
                string strDatabaseType = SqlDataHelper.GetDatabaseType();
                DeviceStatus DS = null;
                if (IsNewDevice == false) // Old devices like F910, M2000
                    DS = GetDeviceStatusFromString(strDeviceStatus);
                else
                    DS = GetNewDeviceStatusFromJsonString(strDeviceStatus); // get device status from Json string like VF1000
                new PMFDServiceDAL.CommonDBLayer().UpdateDeviceStatus(strDatabaseType, ConnStr, DS);
            }
            catch (Exception ex) // failed 
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "DeviceStatusManager.UpdateDeviceStatus( " + ex.Message + ")");
                success = false;
            }
            finally { }
            return success;
        }
        #endregion OLD DEVICES LIKE F910, F710 

        #region NEW DEVICE FUNCTIONS LIKE VF1000
        private DeviceStatus GetNewDeviceStatusFromJsonString(string StrDeviceStatus)
        {
            string DeviceStatusJson = StrDeviceStatus.Replace("PARAM", "DeviceInfoParam"); // replace the PARAM the class is already there in the GetRequest reply
            NewDeviceInfo DInfo = new Json().ConvertJSonToObject<NewDeviceInfo>(DeviceStatusJson);// convert Json to class object           
            Comm cm = new Comm();
            DeviceStatus DS = new DeviceStatus();
            DS.CPUID = DInfo.DeviceInfoParam.deviceInfo.sn;
            DS.DeviceAdd = "00";
            DS.EstbCode = "00000001";
            DS.DeviceType = "VF"; // new device type
            DS.SubType = DInfo.DeviceInfoParam.deviceInfo.type;// "FFFFFFFF";
            DS.DeviceStat = DInfo.DeviceInfoParam.deviceInfo.alg_edition;
            DS.UncapRec = 0;
            DS.EnrolledUser = Int32.Parse(DInfo.DeviceInfoParam.deviceInfo.real_faceregist);
            DS.TotalUser = Int32.Parse(DInfo.DeviceInfoParam.deviceInfo.real_facerecord);
            DS.IPAddress = DInfo.DeviceInfoParam.deviceInfo.ip;
            DS.Firmware = DInfo.DeviceInfoParam.deviceInfo.edition;
            return DS;
        }
        #endregion NEW DEVICE FUNCTIONS LIKE VF1000
    }
}
