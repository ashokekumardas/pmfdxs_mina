﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace PMFDServiceDAL
{
    public partial class SqlDataHelper
    {
        public static Guid GetGuid(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return Guid.Empty; }
            return (Guid)rdr[index];
        }

        public static string GetString(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return string.Empty; }
            return (string)rdr[index];
        }

        public static char GetChar(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return ' '; }
            return Convert.ToChar(rdr[index]);
        }

        public static int GetInt(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return 0; }
            return Convert.ToInt32(rdr[index]);
        }

        public static Int64 GetBigInt(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return 0; }
            return Convert.ToInt64(rdr[index]);
        }
        public static Int16 GetShort(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return 0; }
            return Convert.ToInt16(rdr[index]);
        }

        public static DateTime GetDateTime(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
            { return DateTime.MinValue; }
            return Convert.ToDateTime(rdr[index]);
        }

        public static TimeSpan GetTime(IDataReader rdr, string ColumnName)
        {
            int index = rdr.GetOrdinal(ColumnName);
            if (rdr.IsDBNull(index) == true)
                return TimeSpan.MinValue;
            return (TimeSpan)rdr[index];
        }

        /// <summary> 20-Aug-2018
        /// get the database type from the config file. default SQL Server
        /// </summary>
        /// <returns></returns>
        public static DatabaseType GetDatabaseType(string strDbType)
        {
            DatabaseType rtnDatabaseType = DatabaseType.MS_SQL_Server;// 20-Aug-2018 default value
            if (string.IsNullOrWhiteSpace( strDbType)==false  )
            {
                switch (strDbType.ToUpper())
                {
                    case "ORACLE":
                        rtnDatabaseType = DatabaseType.Oracle;
                        break;
                    case "SQLSERVER":
                        rtnDatabaseType = DatabaseType.MS_SQL_Server;
                        break;
                    case "MSACCESS":
                        rtnDatabaseType = DatabaseType.MS_Access;
                        break;
                    default:
                        rtnDatabaseType = DatabaseType.MS_SQL_Server;
                        break;
                }
            }
            return rtnDatabaseType;
        }
    }
}
