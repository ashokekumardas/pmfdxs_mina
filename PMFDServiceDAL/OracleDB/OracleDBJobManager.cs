﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMFDServiceDAL.OracleDB
{
    public class OracleDBJobManager
    {
        public void GetNext25JobsForThisCPUID(string ConnStr, string CPUID, List<Job> ListOfJob)
        {
            bool success = true;
            string ErrRpt = string.Empty;
            using (OracleConnection OraConn = new OracleConnection(ConnStr))
            {
                try
                {
                    OraConn.Open();
                    if (OraConn.State == ConnectionState.Open)
                    {
                        OracleCommand sqlcmd = new OracleCommand("Select * from (Select JobID,RefNo,CPUID,CommandNo," +
                                              "Command,CommandData,Priority from tblTjob where CPUID='" + CPUID + "' " +
                                               "and FinishedOn is NULL AND SENDTO='T') Where rownum <=25", OraConn);
                        //sqlcmd.Parameters.AddWithValue("CPUID", CPUID);
                        //And " +
                        //                                   "([ExecutionDate] is null) " +
                        //"(" +
                        //"((convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "' between [ExecutionFromTime] And [ExecutionToTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime] And [ExecutionFromTime]>[ExecutionToTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'<= [ExecutionToTime] And [ExecutionFromTime]>[ExecutionToTime])  " +
                        //") And [ExecutionDate] Is Not Null And [ExecutionFromTime] Is Not Null And [ExecutionToTime] Is Not Null) or " +
                        //"(" +
                        //" ((convert(varchar(8),[ExecutionDate],112)= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "')) And [ExecutionToTime] Is NULL )) " +
                        //" order by [Priority],[RefNo],[CreatedOn],[CommandNo]");
                        //sqlcmd.Connection = sqlconn;
                        using (OracleDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Job oJob = CommonDBLayer.GetJobFromReader(reader);
                                ListOfJob.Add(oJob);
                            }
                        }
                    }
                }
                catch (Exception ex) // failed to write into job table
                {
                    ErrRpt = ex.Message.ToString();
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)
                        OraConn.Close();
                    if (success == false)
                        throw new Exception("OracleDBJobManager.GetNext25JobsForThisCPUID( " + ErrRpt + " )");
                }
            }
        }

        public void GetNewJobsForThisCPUID(string ConnStr, string CPUID, List<Job> ListOfJob,int JobCount)
        {
            bool success = true;
            string ErrRpt = string.Empty;
            using (OracleConnection OraConn = new OracleConnection(ConnStr))
            {
                try
                {
                    OraConn.Open();
                    if (OraConn.State == ConnectionState.Open)
                    {
                        OracleCommand sqlcmd = new OracleCommand("Select * from (Select JobID,RefNo,CPUID,CommandNo," +
                                              "Command,CommandData,Priority from tblTjob where CPUID='" + CPUID + "' " +
                                               "and FinishedOn is NULL AND SENDTO='T') Where rownum <="+JobCount.ToString(), OraConn);
                        //sqlcmd.Parameters.AddWithValue("CPUID", CPUID);
                        //And " +
                        //                                   "([ExecutionDate] is null) " +
                        //"(" +
                        //"((convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "' between [ExecutionFromTime] And [ExecutionToTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime] And [ExecutionFromTime]>[ExecutionToTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'<= [ExecutionToTime] And [ExecutionFromTime]>[ExecutionToTime])  " +
                        //") And [ExecutionDate] Is Not Null And [ExecutionFromTime] Is Not Null And [ExecutionToTime] Is Not Null) or " +
                        //"(" +
                        //" ((convert(varchar(8),[ExecutionDate],112)= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime]) or " +
                        //"(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "')) And [ExecutionToTime] Is NULL )) " +
                        //" order by [Priority],[RefNo],[CreatedOn],[CommandNo]");
                        //sqlcmd.Connection = sqlconn;
                        using (OracleDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Job oJob = CommonDBLayer.GetJobFromReader(reader);
                                ListOfJob.Add(oJob);
                            }
                        }
                    }
                }
                catch (Exception ex) // failed to write into job table
                {
                    ErrRpt = ex.Message.ToString();
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)
                        OraConn.Close();
                    if (success == false)
                        throw new Exception("OracleDBJobManager.GetNewJobsForThisCPUID( " + ErrRpt + " )");
                }
            }
        }
        public void InsertJob(string ConnectionString, Job oJob)
        {
            bool success = true;
            string ErrMsg = string.Empty; //
            try
            {
                OracleConnection OraConn = new OracleConnection(ConnectionString);  // odbc connection
                OraConn.Open();  // open the database connection
                string odbcCommand = string.Empty;
                odbcCommand = "Insert into tblTJob (JobID,RefNo,CPUID,CommandNo,Command,CommandData,SendTo," +
                              "CreatedOn,CreatedBy)" +
                              " Values (:JobID,:RefNo,:CPUID,:CommandNo,:Command,:CommandData,'S'," +
                              "TO_DATE(:CreatedOn,'DD-MM-YYYY HH24:MI:SS'),:CreatedBy)";
                //odbcCommand = "Insert into tblTJob (JobID,RefNo,CPUID,CommandNo,Command,CommandData,SendTo,CreatedBy)" +
                //              " Values (:JobID,:RefNo,:CPUID,:CommandNo,:Command,:CommandData,'S',:CreatedBy)";

                OracleCommand sqlcmdattn = new OracleCommand(odbcCommand, OraConn);
                sqlcmdattn.Parameters.AddWithValue("JobID", Guid.NewGuid().ToString("N"));
                sqlcmdattn.Parameters.AddWithValue("RefNo", oJob.RefNo);
                sqlcmdattn.Parameters.AddWithValue("CPUID", oJob.CPUID);
                sqlcmdattn.Parameters.AddWithValue("CommandNo", oJob.CommandNo);
                sqlcmdattn.Parameters.AddWithValue("Command", oJob.Command);
                sqlcmdattn.Parameters.Add("CommandData", OracleType.Clob).Direction = ParameterDirection.Input;
                sqlcmdattn.Parameters["CommandData"].Value = oJob.CommandData;
                //sqlcmdattn.Parameters.AddWithValue("CommandData", oJob.CommandData);
                sqlcmdattn.Parameters.AddWithValue("CreatedOn", DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss"));
                sqlcmdattn.Parameters.AddWithValue("CreatedBy", oJob.CreatedBy);

                try
                {
                    sqlcmdattn.ExecuteNonQuery();
                }
                catch (OracleException insertex)
                {
                    ErrMsg = insertex.Message;
                    success = false;
                }
                catch (Exception GenEx)
                {
                    ErrMsg = GenEx.Message;
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)  // if connection is open then close the connction
                        OraConn.Close();
                }
            }
            catch (OracleException ODBCEx)
            {
                ErrMsg = ODBCEx.Message;
                success = false;
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message;
                success = false;
            }
            finally
            {
                if (success == false)
                    throw new Exception("OracleDBJobManager.InsertJob( " + ErrMsg + " )");
            }
        }

        public void UpdateFinishedJob(string ConnStr, Job oJob)
        {
            bool success = true;
            string ErrorMessage = string.Empty;
            using (OracleConnection OraConn = new OracleConnection(ConnStr))
            {
                try
                {

                    OraConn.Open();
                    if (OraConn.State == ConnectionState.Open)
                    {
                        OracleCommand Oracmd = new OracleCommand();
                        Oracmd.Connection = OraConn;
                        Oracmd.CommandText = "Update tblTjob set FinishedOn=TO_DATE(:FinishedOn,'DD-MM-YYYY HH24:MI:SS')" +
                            (string.IsNullOrWhiteSpace(oJob.ErrCode) ? "" : ",ErrCode=:ErrCode") + " where RefNo=:RefNo AND SENDTO='T'";
                        Oracmd.Parameters.AddWithValue("FinishedOn", DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss"));
                        if (string.IsNullOrWhiteSpace(oJob.ErrCode )==false) // some value present
                            Oracmd.Parameters.AddWithValue("ErrCode", oJob.ErrCode);
                        Oracmd.Parameters.AddWithValue("RefNo", oJob.RefNo);

                        int intSuccess = Oracmd.ExecuteNonQuery();
                        success = true;
                    }
                }

                catch (Exception Exp)
                {
                    success = false;
                    ErrorMessage = Exp.Message;                    
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)
                        OraConn.Close(); 
                    if(success==false)
                        throw new Exception("OracleDBJobManager.UpdateFinishedJob( " + ErrorMessage + ")");
                }
            }
        }

    }
}
