﻿using System;
using System.Data;
using System.Data.OracleClient;

namespace PMFDServiceDAL.OracleDB
{
    public class OracleDBAttendanceManager
    {
        public void InsertAttendanceData(string ConnectionString,Attendance oAttnJob)
        {
            bool success = true;
            string ErrMsg = string.Empty;
            try
            {
                OracleConnection OraConn = new OracleConnection(ConnectionString);  // odbc connection
                OraConn.Open();  // open the database connection
                string odbcCommand = string.Empty;
                char[] chrCommandData = oAttnJob.CommandData.ToCharArray();// conver to to char array to insert into CLOB field
                odbcCommand = "Insert into tblTAttendanceJob (JobID,RefNo,CPUID,CommandNo,Command,CommandData,SendTo,CreatedBy)" +
                              " Values (:JobID,:RefNo,:CPUID,:CommandNo,:Command,:CommandData,'S',:CreatedBy)";
                OracleCommand sqlcmdattn = new OracleCommand(odbcCommand, OraConn);
                sqlcmdattn.Parameters.Add("JobID",OracleType.VarChar).Direction=ParameterDirection.Input;
                sqlcmdattn.Parameters.Add("RefNo", OracleType.VarChar).Direction = ParameterDirection.Input;                
                sqlcmdattn.Parameters.Add("CPUID", OracleType.VarChar).Direction=ParameterDirection.Input;                
                sqlcmdattn.Parameters.Add("CommandNo", OracleType.Number).Direction=ParameterDirection.Input;                
                sqlcmdattn.Parameters.Add("Command",OracleType.Number).Direction=ParameterDirection.Input;
                sqlcmdattn.Parameters.Add("CommandData", OracleType.Clob).Direction = ParameterDirection.Input;                
                sqlcmdattn.Parameters.Add("CreatedBy",OracleType.Number).Direction=ParameterDirection.Input;
                //////////////// Parameter value ////////////////////////////                                
                sqlcmdattn.Parameters["JobID"].Value = Guid.NewGuid().ToString("N");
                sqlcmdattn.Parameters["RefNo"].Value = oAttnJob.RefNo;
                sqlcmdattn.Parameters["CPUID"].Value = oAttnJob.CPUID;
                sqlcmdattn.Parameters["CommandNo"].Value = oAttnJob.CommandNo;
                sqlcmdattn.Parameters["Command"].Value = oAttnJob.Command;
                sqlcmdattn.Parameters["CommandData"].Value = chrCommandData;
                sqlcmdattn.Parameters["CreatedBy"].Value = oAttnJob.CreatedBy;                

                try
                {
                    sqlcmdattn.ExecuteNonQuery();
                }
                catch (OracleException insertex)
                {
                    ErrMsg= insertex.Message ;
                    success = false;
                }
                catch (Exception GenEx)
                {
                    ErrMsg = GenEx.Message;
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)  // if connection is open then close the connction
                        OraConn.Close();
                }
            }
            catch (OracleException ODBCEx)
            {
                ErrMsg = ODBCEx.Message;
                success = false;
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message;
                success = false;
            }
            finally
            {
                if (success == false)// 21-Aug-2018 some error occured
                    throw new Exception("OracleDBAttendanceManager.InsertAttendanceData( " + ErrMsg + " )");
            }
        }

    }
}
