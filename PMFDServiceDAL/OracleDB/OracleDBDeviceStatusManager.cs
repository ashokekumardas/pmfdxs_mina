﻿using System;
using System.Data;
using System.Data.OracleClient;

namespace PMFDServiceDAL.OracleDB
{
    public class OracleDBDeviceStatusManager
    {
        public DeviceStatus GetOpenDeviceStatusByCPUID(string ConnStr, string cpuid)
        {
            DeviceStatus result = new DeviceStatus();
            using (OracleConnection OraConn = new OracleConnection(ConnStr))
            {
                OraConn.Open();
                if (OraConn.State == ConnectionState.Open)
                {
                    OracleCommand sqlcmd = new OracleCommand("Select * from (Select CPUID,DeviceAdd,EstbCode,Type,SubType,DeviceStat,IPAddress,CardPrefix,EmpIDPrefix," +
                                                        "GPRSSignal,GPRSOperator,GPRSTower,SIMNo,UncapRec,EnrolledUser,TotalUser,TotalTemplate,Firmware," +
                                                        "FirmwareID,TimeZone,LastSyncWithServer from tblTDeviceStatus where CPUID='" + cpuid + "' " +
                                                        " And ClosedOn is NULL) Where rownum <=1 ", OraConn);
                    using (OracleDataReader reader = sqlcmd.ExecuteReader())
                    {
                        if (reader.Read())
                            result = CommonDBLayer.GetDeviceStatusFromReader(reader);
                    }
                }
                return result;
            }
        }

        public void InsertDeviceStatus(string ConnectionString, DeviceStatus DS)
        {
            bool success = true;
            string ErrorMessage = string.Empty; //
            try
            {
                OracleConnection OraConn = new OracleConnection(ConnectionString);  // odbc connection
                OraConn.Open();  // open the database connection
                string odbcCommand = string.Empty;
                odbcCommand = "Insert into tblTDeviceStatus (CPUID,DeviceAdd,EstbCode,Type,SubType,DeviceStat,IPAddress," +
                               "CardPrefix,EmpIDPrefix,GPRSSignal,GPRSOperator,GPRSTower,SIMNo,UncapRec,EnrolledUser," +
                               "TotalUser,TotalTemplate,Firmware,FirmwareID,TimeZone,LastSyncWithServer) " +
                               "Values (:CPUID,:DeviceAdd,:EstbCode,:Type,:SubType,:DeviceStat,:IPAddress," +
                               ":CardPrefix,:EmpIDPrefix,:GPRSSignal,:GPRSOperator,:GPRSTower,:SIMNo,:UncapRec,:EnrolledUser," +
                               ":TotalUser,:TotalTemplate,:Firmware,:FirmwareID,:TimeZone," +
                               "TO_DATE(:LastSyncWithServer,'DD-MM-YYYY HH24:MI:SS'))";
                OracleCommand sqlcmdattn = new OracleCommand(odbcCommand, OraConn);
                sqlcmdattn.Parameters.AddWithValue("CPUID", DS.CPUID);
                sqlcmdattn.Parameters.AddWithValue("DeviceAdd", DS.DeviceAdd);
                sqlcmdattn.Parameters.AddWithValue("EstbCode", DS.EstbCode );
                sqlcmdattn.Parameters.AddWithValue("Type", DS.DeviceType);
                sqlcmdattn.Parameters.AddWithValue("SubType", DS.SubType);
                sqlcmdattn.Parameters.AddWithValue("DeviceStat", DS.DeviceStat);
                sqlcmdattn.Parameters.AddWithValue("IPAddress", DS.IPAddress);
                sqlcmdattn.Parameters.AddWithValue("CardPrefix", DS.CardPrefix);
                sqlcmdattn.Parameters.AddWithValue("EmpIDPrefix", DS.EmpIDPrefix);
                sqlcmdattn.Parameters.AddWithValue("GPRSSignal", DS.GPRSSignal);
                sqlcmdattn.Parameters.AddWithValue("GPRSOperator", DS.GPRSOperator);
                sqlcmdattn.Parameters.AddWithValue("GPRSTower", DS.GPRSTower);
                sqlcmdattn.Parameters.AddWithValue("SIMNo", DS.SIMNo);
                sqlcmdattn.Parameters.AddWithValue("UncapRec", DS.UncapRec);
                sqlcmdattn.Parameters.AddWithValue("EnrolledUser", DS.EnrolledUser);
                sqlcmdattn.Parameters.AddWithValue("TotalUser", DS.TotalUser);
                sqlcmdattn.Parameters.AddWithValue("TotalTemplate", DS.TotalTemplate);
                sqlcmdattn.Parameters.AddWithValue("Firmware", DS.Firmware);
                sqlcmdattn.Parameters.AddWithValue("FirmwareID", DS.FirmwareID);
                sqlcmdattn.Parameters.AddWithValue("TimeZone", DS.TimeZone);
                sqlcmdattn.Parameters.AddWithValue("LastSyncWithServer", DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss"));
                //sqlcmdattn.Parameters.AddWithValue("CreatedBy", oAttnJob.CreatedBy);

                try
                {
                    sqlcmdattn.ExecuteNonQuery();
                }
                catch (OracleException insertex)
                {
                    ErrorMessage = insertex.Message;
                    success = false;
                }
                catch (Exception GenEx)
                {
                    ErrorMessage = GenEx.Message;
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)  // if connection is open then close the connction
                        OraConn.Close();
                }
            }
            catch (OracleException ODBCEx)
            {
                ErrorMessage = ODBCEx.Message;
                success = false;
            }
            catch (Exception Ex)
            {
                ErrorMessage = Ex.Message;
                success = false;
            }
            finally
            {
                if (success == false)
                    throw new Exception("OracleDBDeviceStatusManager.InsertDeviceStatus( " + ErrorMessage + " )");
            }
        }

        public void UpdateDeviceStatus(string ConnectionString, DeviceStatus DS)
        {
            bool success = true;
            string ErrMsg = string.Empty; //
            try
            {
                OracleConnection OraConn = new OracleConnection(ConnectionString);  // odbc connection
                OraConn.Open();  // open the database connection
                string odbcCommand = string.Empty;
                odbcCommand = "Update tblTDeviceStatus set DeviceAdd=:DeviceAdd,EstbCode=:EstbCode,Type=:Type," +
                              "SubType=:SubType,DeviceStat=:DeviceStat,IPAddress=:IPAddress," +
                               "CardPrefix=:CardPrefix,EmpIDPrefix=:EmpIDPrefix,GPRSSignal=:GPRSSignal," +
                               "GPRSOperator=:GPRSOperator,GPRSTower=:GPRSTower,SIMNo=:SIMNo,UncapRec=:UncapRec," +
                               "EnrolledUser=:EnrolledUser," +
                               "TotalUser=:TotalUser,TotalTemplate=:TotalTemplate,Firmware=:Firmware,FirmwareID=:FirmwareID," +
                               "LastSyncWithServer=TO_DATE(:LastSyncWithServer,'DD-MM-YYYY HH24:MI:SS') " +
                               " Where CPUID=:CPUID and ClosedOn is NULL";
                               ///"TimeZone=:TimeZone,LastSyncWithServer=TO_DATE(:LastSyncWithServer,'DD-MM-YYYY HH24:MI:SS') " + // 29-Mar-2019 timezone parameter omitted
                OracleCommand sqlcmdattn = new OracleCommand(odbcCommand, OraConn);
                sqlcmdattn.Parameters.AddWithValue("CPUID", DS.CPUID);
                sqlcmdattn.Parameters.AddWithValue("DeviceAdd", DS.DeviceAdd);
                sqlcmdattn.Parameters.AddWithValue("EstbCode", DS.EstbCode);
                sqlcmdattn.Parameters.AddWithValue("Type", DS.DeviceType);
                sqlcmdattn.Parameters.AddWithValue("SubType", DS.SubType);
                sqlcmdattn.Parameters.AddWithValue("DeviceStat", DS.DeviceStat);
                sqlcmdattn.Parameters.AddWithValue("IPAddress", DS.IPAddress);
                sqlcmdattn.Parameters.AddWithValue("CardPrefix", DS.CardPrefix);
                sqlcmdattn.Parameters.AddWithValue("EmpIDPrefix", DS.EmpIDPrefix);
                sqlcmdattn.Parameters.AddWithValue("GPRSSignal", DS.GPRSSignal);
                sqlcmdattn.Parameters.AddWithValue("GPRSOperator", DS.GPRSOperator);
                sqlcmdattn.Parameters.AddWithValue("GPRSTower", DS.GPRSTower);
                sqlcmdattn.Parameters.AddWithValue("SIMNo", DS.SIMNo);
                sqlcmdattn.Parameters.AddWithValue("UncapRec", DS.UncapRec);
                sqlcmdattn.Parameters.AddWithValue("EnrolledUser", DS.EnrolledUser);
                sqlcmdattn.Parameters.AddWithValue("TotalUser", DS.TotalUser);
                sqlcmdattn.Parameters.AddWithValue("TotalTemplate", DS.TotalTemplate);
                sqlcmdattn.Parameters.AddWithValue("Firmware", DS.Firmware);
                sqlcmdattn.Parameters.AddWithValue("FirmwareID", DS.FirmwareID);
                //sqlcmdattn.Parameters.AddWithValue("TimeZone", DS.TimeZone);// 29-Mar-2019 timezone parameter omitted
                sqlcmdattn.Parameters.AddWithValue("LastSyncWithServer", DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss"));
                //sqlcmdattn.Parameters.AddWithValue("CreatedBy", oAttnJob.CreatedBy);

                try
                {
                    sqlcmdattn.ExecuteNonQuery();
                }
                catch (OracleException insertex)
                {
                    ErrMsg = insertex.Message;
                    success = false;
                }
                catch (Exception GenEx)
                {
                    ErrMsg = GenEx.Message;
                    success = false;
                }
                finally
                {
                    if (OraConn.State == ConnectionState.Open)  // if connection is open then close the connction
                        OraConn.Close();
                }
            }
            catch (OracleException ODBCEx)
            {
                ErrMsg = ODBCEx.Message;
                success = false;
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message;
                success = false;
            }
            finally
            {
                if (success == false)
                    throw new Exception("OracleDBDeviceStatusManager.UpdateDeviceStatus( " + ErrMsg + " )");
            }
        }

    }
}
