﻿using PMFDServiceDAL.OracleDB;
using PMFDServiceDAL.SQLServerDB;
using System;
using System.Collections.Generic;
using System.Data;

namespace PMFDServiceDAL
{
    public class CommonDBLayer
    {
        public void InsertAttendanceData(string strDataBaseType, string ConnectionString, Attendance AttnJob)
        {
            bool Success = true;
            string ErrorMessage = string.Empty;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBAttendanceManager().InsertAttendanceData(ConnectionString, AttnJob);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBAttendanceManager().InsertAttendanceData(ConnectionString, AttnJob);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }
            }
            catch (Exception GenEx) { Success = false; ErrorMessage = GenEx.Message; }// 18-Jun-2020 attendance data insert failed success = false was missing
            finally
            {
                if (Success == false) //21-Aug-2018 some exception
                    throw new Exception("CommonDBLayer.InsertAttendanceData(" + ErrorMessage + ")");
            }
        }

        #region DEVICE STATUS
        public void InsertDeviceStatus(string strDataBaseType, string ConnectionString, DeviceStatus DS)
        {
            bool Success = true;
            string ErrorMessage = string.Empty;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBDeviceStatusManager().InsertDeviceStatus(ConnectionString, DS);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBDeviceStatusManager().InsertDeviceStatus(ConnectionString, DS);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }
            }
            catch (Exception GenEx) { Success = false; ErrorMessage = GenEx.Message; }
            finally
            {
                if (Success == false)
                    throw new Exception("CommonDBLayer.InsertDeviceStatus(" + ErrorMessage + ")");
            }
        }

        public void UpdateDeviceStatus(string strDataBaseType, string ConnectionString, DeviceStatus DS)
        {
            bool Success = true;
            string ErrorMessage = string.Empty;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBDeviceStatusManager().UpdateDeviceStatus(ConnectionString, DS);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBDeviceStatusManager().UpdateDeviceStatus(ConnectionString, DS);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }
            }
            catch (Exception GenEx) { Success = false; ErrorMessage = GenEx.Message; }
            finally
            {
                if (Success == false)
                    throw new Exception("CommonDBLayer.UpdateDeviceStatus(" + ErrorMessage + ")");
            }
        }

        public DeviceStatus GetDeviceStatusByCPUID(string strDataBaseType, string ConnectionString, string CPUID)
        {
            bool Success = true;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            DeviceStatus DS = new DeviceStatus();
            string ErrorMessage;
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        DS = new OracleDBDeviceStatusManager().GetOpenDeviceStatusByCPUID(ConnectionString, CPUID);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        DS = new SQLServerDBDeviceStatusManager().GetOpenDeviceStatusByCPUID(ConnectionString, CPUID);
                        break;
                    default:
                        Success = false;
                        break;
                }
                return DS;
            }
            catch (Exception GenEx) { throw new Exception("CommonDBLayer.GetDeviceStatusByCPUID(" + GenEx.Message + ")"); }
        }

        internal static DeviceStatus GetDeviceStatusFromReader(IDataReader DataReader)
        {
            DeviceStatus oTerminal = new DeviceStatus();
            oTerminal.CPUID = SqlDataHelper.GetString(DataReader, "CPUID");
            oTerminal.DeviceAdd = SqlDataHelper.GetString(DataReader, "DeviceAdd");
            oTerminal.EstbCode = SqlDataHelper.GetString(DataReader, "EstbCode");
            oTerminal.DeviceType = SqlDataHelper.GetString(DataReader, "Type");
            oTerminal.SubType = SqlDataHelper.GetString(DataReader, "SubType");

            //oTerminal.CaptureMode = SqlDataHelper.GetChar(DataReader, "CaptureMode");
            //oTerminal.TemplateType = SqlDataHelper.GetChar(DataReader, "TemplateType");
            //oTerminal.TemplateStoringMode = SqlDataHelper.GetChar(DataReader, "TemplateStoringMode");

            oTerminal.DeviceStat = SqlDataHelper.GetString(DataReader, "DeviceStat");
            oTerminal.IPAddress = SqlDataHelper.GetString(DataReader, "IPAddress");
            oTerminal.CardPrefix = SqlDataHelper.GetString(DataReader, "CardPrefix");
            oTerminal.EmpIDPrefix = SqlDataHelper.GetString(DataReader, "EmpIDPrefix");
            oTerminal.GPRSSignal = SqlDataHelper.GetShort(DataReader, "GPRSSignal");
            oTerminal.GPRSOperator = SqlDataHelper.GetString(DataReader, "GPRSOperator");
            oTerminal.GPRSTower = SqlDataHelper.GetString(DataReader, "GPRSTower");
            oTerminal.SIMNo = SqlDataHelper.GetString(DataReader, "SIMNo");
            oTerminal.UncapRec = SqlDataHelper.GetInt(DataReader, "UncapRec");
            oTerminal.EnrolledUser = SqlDataHelper.GetInt(DataReader, "EnrolledUser");
            oTerminal.TotalUser = SqlDataHelper.GetInt(DataReader, "TotalUser");
            oTerminal.TotalTemplate = SqlDataHelper.GetInt(DataReader, "TotalTemplate");
            oTerminal.Firmware = SqlDataHelper.GetString(DataReader, "Firmware");
            //oTerminal.FirmwareID = SqlDataHelper.GetString(DataReader, "FirmwareID");  
            oTerminal.TimeZone = SqlDataHelper.GetString(DataReader, "TimeZone");
            oTerminal.LastSyncWithServer = SqlDataHelper.GetDateTime(DataReader, "LastSyncWithServer");
            return oTerminal;
        }
        #endregion DEVICE STATUS

        #region DEVICE JOB

        public bool GetDeviceJobByCPUID(string strDataBaseType, string ConnectionString, string CPUID, List<Job> ListOfJobs)
        {
            bool Success = true;
            string ErrorMessage = string.Empty;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBJobManager().GetNext25JobsForThisCPUID(ConnectionString, CPUID, ListOfJobs);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBJobManager().GetNext25JobsForThisCPUID(ConnectionString, CPUID, ListOfJobs);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }
                return Success;
            }
            catch (Exception GenEx) { ErrorMessage = GenEx.Message; Success = false; }
            finally
            {
                if (Success == false)
                    throw new Exception("CommonDBLayer.GetDeviceJobByCPUID(" + ErrorMessage + ")");
            }
            return Success;
        }

        public bool GetNewDeviceJobByCPUID(string strDataBaseType, string ConnectionString, string CPUID, List<Job> ListOfJobs,int JobCount)
        {
            bool Success = true;
            string ErrorMessage = string.Empty;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBJobManager().GetNewJobsForThisCPUID(ConnectionString, CPUID, ListOfJobs, JobCount);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBJobManager().GetNewJobsForThisCPUID(ConnectionString, CPUID, ListOfJobs,JobCount);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }
                return Success;
            }
            catch (Exception GenEx) { ErrorMessage = GenEx.Message; Success = false; }
            finally
            {
                if (Success == false)
                    throw new Exception("CommonDBLayer.GetNewDeviceJobByCPUID(" + ErrorMessage + ")");
            }
            return Success;
        }
        public bool InsertDeviceJob(string strDataBaseType, string ConnectionString,Job oJob)
        {
            bool Success = true;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            string ErrorMessage=string.Empty;
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBJobManager().InsertJob(ConnectionString, oJob);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBJobManager().InsertJob(ConnectionString, oJob);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }                
            }
            catch (Exception GenEx) { ErrorMessage = GenEx.Message; Success = false; }
            finally
            {
                if(Success==false)
                    throw new Exception("CommonDBLayer.InsertDeviceJob( "+ ErrorMessage+ " )");
            }
            return Success;
        }

        public bool UpdateDeviceJob(string strDataBaseType, string ConnectionString, Job oJob)
        {
            bool Success = true;
            DatabaseType DBType = SqlDataHelper.GetDatabaseType(strDataBaseType);
            string ErrorMessage=string.Empty;
            try
            {
                switch (DBType)
                {
                    case DatabaseType.Oracle:
                        new OracleDBJobManager().UpdateFinishedJob(ConnectionString, oJob);
                        break;
                    case DatabaseType.MS_SQL_Server:
                        new SQLServerDBJobManager().UpdateFinishedJob(ConnectionString, oJob);
                        break;
                    default:
                        ErrorMessage = "Unsupported Database type";
                        Success = false;
                        break;
                }                
            }
            catch (Exception GenEx) { ErrorMessage=GenEx.Message;Success=false; }
            finally{if(Success==false)throw new Exception("CommonDBLayer.UpdateDeviceJob(" + ErrorMessage + ")");}
            return Success;
        }


        public static Job GetJobFromReader(IDataReader datareader )
        {
            Job oJob = new Job();
//            oJob.JobID = SqlDataHelper.GetGuid(datareader, "JobID");
            oJob.RefNo = SqlDataHelper.GetString(datareader, "RefNo");
            oJob.CPUID = SqlDataHelper.GetString(datareader, "CPUID");
            oJob.CommandNo = SqlDataHelper.GetShort(datareader, "CommandNo");
            oJob.Command = SqlDataHelper.GetString(datareader, "Command");
            oJob.CommandData = SqlDataHelper.GetString(datareader, "CommandData");
            oJob.Priority = SqlDataHelper.GetChar(datareader, "Priority");
            return oJob;
        }
        #endregion DEVICE JOB
    }
}
