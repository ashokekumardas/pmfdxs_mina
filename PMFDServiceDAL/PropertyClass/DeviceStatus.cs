﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMFDServiceDAL
{
    public partial class DeviceStatus
    {

        #region Constructor
        public DeviceStatus()
        {
            CPUID = string.Empty;
            DeviceAdd = "00";
            EstbCode = "00000001";
            DeviceType = string.Empty;
            SubType = "FFFFFFFF";
            DeviceStat = string.Empty;
            IPAddress = string.Empty;
            CardPrefix = string.Empty;
            EmpIDPrefix = string.Empty;
            GPRSSignal = 0;
            GPRSOperator = string.Empty;
            GPRSTower = string.Empty;
            SIMNo = string.Empty;
            UncapRec = 0;
            EnrolledUser = 0;
            TotalUser = 0;
            TotalTemplate = 0;
            Firmware = string.Empty;
            FirmwareID = string.Empty;
            TimeZone = "+053000";
            LastSyncWithServer = DateTime.UtcNow;
        }
        #endregion

        #region Properties
        public string CPUID { get; set; }
        public string DeviceAdd { get; set; }
        public string EstbCode { get; set; }
        public string DeviceType { get; set; }
        public string SubType { get; set; }
        public string DeviceStat { get; set; }
        public string IPAddress { get; set; }
        public string CardPrefix { get; set; }
        public string EmpIDPrefix { get; set; }
        public Int16 GPRSSignal { get; set; }
        public string GPRSOperator { get; set; }
        public string GPRSTower { get; set; }
        public string SIMNo { get; set; }
        public Int32 UncapRec { get; set; }
        public Int32 EnrolledUser { get; set; }
        public Int32 TotalUser { get; set; }
        public Int32 TotalTemplate { get; set; }
        public string Firmware { get; set; }
        public string FirmwareID { get; set; }
        public string TimeZone { get; set; }
        public DateTime LastSyncWithServer { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ClosedOn { get; set; }
        #endregion

    }
}
