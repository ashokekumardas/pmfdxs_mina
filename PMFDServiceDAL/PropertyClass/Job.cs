﻿using System;

namespace PMFDServiceDAL
{
    public class Job
    {
        public Job() { RefNo = string.Empty; CreatedBy = 910; ErrCode=string.Empty; ThisSessionJobNo = 1; }

        public Guid JobID { get; set; }
        public string RefNo { get; set; }
        public string CPUID { get; set; }
        public int CommandNo { get; set; }
        public string Command { get; set; }
        public string CommandData { get; set; }
        public char SendTo { get; set; }
        public char Priority { get; set; }
        public DateTime FinishedOn { get; set; }
        public string ErrCode { get; set; }
        public int CreatedBy { get; set; }
        public Int16 ThisSessionJobNo { get; set; } // job count number that is passed to the device in this session. this is to track multiple jobs in single session
    }
}
