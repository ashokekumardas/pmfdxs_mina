﻿using System;

namespace PMFDServiceDAL
{
    public class Attendance
    {
        public Attendance() { RefNo = string.Empty;CPUID = string.Empty; CreatedBy = 910; }
        public Guid JobID { get; set; }
        public string RefNo { get; set; }
        public string CPUID { get; set; }
        public int CommandNo { get; set; }
        public int Command { get; set; }
        public string CommandData { get; set; }
        public char SendTo { get; set; }
        public int CreatedBy { get; set; }
    }
}
