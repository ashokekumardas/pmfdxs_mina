﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PMFDServiceDAL.SQLServerDB
{
    public class SQLServerDBJobManager
    {
        public void GetNext25JobsForThisCPUID(string ConnStr,string CPUID, List<Job> ListOfJob)
        {
            bool success=true;
            string ErrRpt = string.Empty;
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                try
                {

                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        SqlCommand sqlcmd = new SqlCommand("select top 25 * from tblTjob where [CPUID]='" + CPUID + "' and [FinishedOn] is NULL AND [SENDTO]='T'  And " +
                                                           "([ExecutionDate] is null or " +
                        "(" +
                        "((convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "' between [ExecutionFromTime] And [ExecutionToTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime] And [ExecutionFromTime]>[ExecutionToTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'<= [ExecutionToTime] And [ExecutionFromTime]>[ExecutionToTime])  " +
                        ") And [ExecutionDate] Is Not Null And [ExecutionFromTime] Is Not Null And [ExecutionToTime] Is Not Null) or " +
                        "(" +
                        " ((convert(varchar(8),[ExecutionDate],112)= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "')) And [ExecutionToTime] Is NULL )) " +
                        " order by [Priority],[RefNo],[CreatedOn],[CommandNo]");
                        sqlcmd.Connection = sqlconn;
                        using (IDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Job oJob = CommonDBLayer.GetJobFromReader(reader);
                                ListOfJob.Add(oJob);
                            }
                        }
                    }
                }
                catch (Exception ex) // failed to write into job table
                {
                    ErrRpt = ex.Message.ToString();
                    success = false;
                }
                finally
                {
                    if (sqlconn.State == ConnectionState.Open)
                        sqlconn.Close();
                }
            }
            if (success == false)
                throw new Exception("SQLServerDBJobManager.GetNext25JobsForThisCPUID( " + ErrRpt + " )");
        }

        public void GetNewJobsForThisCPUID(string ConnStr, string CPUID, List<Job> ListOfJob,int JobCount)
        {
            bool success = true;
            string ErrRpt = string.Empty;
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                try
                {

                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        SqlCommand sqlcmd = new SqlCommand("select top "+JobCount+" * from tblTjob where [CPUID]='" + CPUID + "' and [FinishedOn] is NULL AND [SENDTO]='T'  And " +
                                                           "([ExecutionDate] is null or " +
                        "(" +
                        "((convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "' between [ExecutionFromTime] And [ExecutionToTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)<= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime] And [ExecutionFromTime]>[ExecutionToTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'<= [ExecutionToTime] And [ExecutionFromTime]>[ExecutionToTime])  " +
                        ") And [ExecutionDate] Is Not Null And [ExecutionFromTime] Is Not Null And [ExecutionToTime] Is Not Null) or " +
                        "(" +
                        " ((convert(varchar(8),[ExecutionDate],112)= '" + DateTime.UtcNow.ToString("yyyyMMdd") + "' And '" + DateTime.UtcNow.ToString("HH:mm:ss") + "'>= [ExecutionFromTime]) or " +
                        "(convert(varchar(8),[ExecutionDate],112)< '" + DateTime.UtcNow.ToString("yyyyMMdd") + "')) And [ExecutionToTime] Is NULL )) " +
                        " order by [Priority],[RefNo],[CreatedOn],[CommandNo]");
                        sqlcmd.Connection = sqlconn;
                        using (IDataReader reader = sqlcmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Job oJob = CommonDBLayer.GetJobFromReader(reader);
                                ListOfJob.Add(oJob);
                            }
                        }
                    }
                }
                catch (Exception ex) // failed to write into job table
                {
                    ErrRpt = ex.Message.ToString();
                    success = false;
                }
                finally
                {
                    if (sqlconn.State == ConnectionState.Open)
                        sqlconn.Close();
                }
            }
            if (success == false)
                throw new Exception("SQLServerDBJobManager.GetNext25JobsForThisCPUID( " + ErrRpt + " )");
        }
        public void InsertJob(string ConnStr,Job oJob)
        {            
            try
            {
                using (SqlConnection sqlconn = new SqlConnection(ConnStr))
                {
                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        using (SqlCommand cmdsql = new SqlCommand())
                        {
                            string dbFields, dbFieldValues;
                            dbFields = "[JobID]";
                            dbFieldValues = "@JobID";
                            cmdsql.Parameters.Add("@JobID", SqlDbType.UniqueIdentifier);
                            cmdsql.Parameters["@JobID"].Value = Guid.NewGuid();
                            if (!oJob.RefNo.Equals(string.Empty))
                            {
                                dbFields += ",[RefNo]";
                                dbFieldValues += ",@RefNo";
                                cmdsql.Parameters.Add("@RefNo", SqlDbType.VarChar, 20);
                                cmdsql.Parameters["@RefNo"].Value = oJob.RefNo;
                            }
                            else
                                throw (new Exception("Job Refrence cant be blank"));
                            if (!oJob.CPUID.Equals(string.Empty))
                            {
                                dbFields += ",[CPUID]";
                                dbFieldValues += ",@CPUId";
                                cmdsql.Parameters.Add("@CPUID", SqlDbType.VarChar, 30);
                                cmdsql.Parameters["@CPUID"].Value = oJob.CPUID;
                            }
                            else
                                throw (new Exception("CPU Id cant be blank"));
                            if (oJob.CommandNo != null)
                            {
                                dbFields += ",[CommandNo]";
                                dbFieldValues += ",@CommandNo";
                                cmdsql.Parameters.Add("@CommandNo", SqlDbType.SmallInt);
                                cmdsql.Parameters["@CommandNo"].Value = oJob.CommandNo;
                            }
                            else
                                throw (new Exception("Command No cant be blank"));
                            if (!oJob.Command.Equals(string.Empty))
                            {
                                dbFields += ",[Command]";
                                dbFieldValues += ",@Command";
                                cmdsql.Parameters.Add("@Command", SqlDbType.VarChar, 5);
                                cmdsql.Parameters["@Command"].Value = oJob.Command;
                            }
                            else
                                throw (new Exception("Command cant be blank"));
                            if (!oJob.CommandData.Equals(string.Empty))
                            {
                                dbFields += ",[CommandData]";
                                dbFieldValues += ",@CommandData";
                                cmdsql.Parameters.Add("@CommandData", SqlDbType.VarChar);
                                cmdsql.Parameters["@CommandData"].Value = oJob.CommandData;
                            }
                            else
                                throw (new Exception("Command Data cant be blank"));
                            if (oJob.SendTo != null)
                            {
                                dbFields += ",[SendTo]";
                                dbFieldValues += ",@SendTo";
                                cmdsql.Parameters.Add("@SendTo", SqlDbType.Char, 1);
                                cmdsql.Parameters["@SendTo"].Value = oJob.SendTo;
                            }
                            else
                                throw (new Exception("SendTo should not be empty."));
                            if (oJob.CreatedBy != 0)
                            {
                                dbFields += ",[CreatedBy]";
                                dbFieldValues += ",@CreatedBy";
                                cmdsql.Parameters.Add("@CreatedBy", SqlDbType.Int);
                                cmdsql.Parameters["@CreatedBy"].Value = oJob.CreatedBy;
                            }
                            cmdsql.Connection = sqlconn;
                            cmdsql.CommandText = "insert into tblTJob (" + dbFields + ") values (" + dbFieldValues + ")";
                            if (cmdsql.ExecuteNonQuery() != 0)
                            {
                                cmdsql.Dispose();
                            }
                            else
                                throw (new Exception("Unable to insert job"));
                        }
                    }
                    else
                        throw (new Exception("Unable to open database"));
                }
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message.ToString()));
            }
        }

        public void UpdateFinishedJob(string ConnStr, Job oJob)
        {
            bool success = true;
            string ErrorMessage = string.Empty;
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                try
                {

                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        SqlCommand sqlcmd = new SqlCommand();
                        sqlcmd.Connection = sqlconn;
                        sqlcmd.CommandText = "Update tblTjob set [FinishedOn]='" + oJob.FinishedOn.ToString("dd-MMM-yyyy HH:mm:ss") + "',[ErrCode]=" + (string.IsNullOrWhiteSpace( oJob.ErrCode )? "null" : "'" + oJob.ErrCode + "'") + " where [RefNo]='" + oJob.RefNo + "' AND [SENDTO]='T'";
                        int intSuccess = sqlcmd.ExecuteNonQuery();
                    }
                }
                catch (Exception Exp)
                {
                    success = false;
                    ErrorMessage = Exp.Message;
                }
                finally
                {
                    if (sqlconn.State == ConnectionState.Open)
                        sqlconn.Close();
                }
            }
            if (success == false)
                throw new Exception("SQLServerDBJobManager.UpdateFinishedJob( " + ErrorMessage + " )");
        }
    }
}
