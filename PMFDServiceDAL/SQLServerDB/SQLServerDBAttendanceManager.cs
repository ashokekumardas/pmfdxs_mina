﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMFDServiceDAL.SQLServerDB
{
    public class SQLServerDBAttendanceManager
    {
        public void InsertAttendanceData(string ConnectionString, Attendance oAttnJob)
        {
            bool success = true;
            string ErrMsg = string.Empty; //
            try
            {
                using (SqlConnection sqlconn = new SqlConnection(ConnectionString))
                {
                    sqlconn.Open();
                    string DBFields = "", DBFieldValues = "";
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.Connection = sqlconn;

                    DBFields = "[JobID]";
                    DBFieldValues = "@JobID";
                    sqlcmd.Parameters.Add("@JobID", SqlDbType.UniqueIdentifier);
                    sqlcmd.Parameters["@JobID"].Value = Guid.NewGuid();

                    DBFields += ",[RefNo]";
                    DBFieldValues += ",@RefNo";
                    sqlcmd.Parameters.Add("@RefNo", SqlDbType.VarChar, 20);
                    sqlcmd.Parameters["@RefNo"].Value = oAttnJob.RefNo;

                    DBFields += ",[CPUID]";
                    DBFieldValues += ",@CPUID";
                    sqlcmd.Parameters.Add("@CPUID", SqlDbType.VarChar, 30);
                    sqlcmd.Parameters["@CPUID"].Value = oAttnJob.CPUID;

                    DBFields += ",[CommandNo]";
                    DBFieldValues += ",@CommandNo";
                    sqlcmd.Parameters.Add("@CommandNo", SqlDbType.SmallInt);
                    sqlcmd.Parameters["@CommandNo"].Value = oAttnJob.CommandNo;

                    DBFields += ",[Command]";
                    DBFieldValues += ",@Command";
                    sqlcmd.Parameters.Add("@Command", SqlDbType.Int);
                    sqlcmd.Parameters["@Command"].Value = oAttnJob.Command;

                    DBFields += ",[CommandData]";
                    DBFieldValues += ",@CommandData";
                    sqlcmd.Parameters.Add("@CommandData", SqlDbType.VarChar);
                    sqlcmd.Parameters["@CommandData"].Value = oAttnJob.CommandData;

                    DBFields += ",[SendTo]";
                    DBFieldValues += ",@SendTo";
                    sqlcmd.Parameters.Add("@SendTo", SqlDbType.VarChar, 1);
                    sqlcmd.Parameters["@SendTo"].Value = oAttnJob.SendTo;

                    DBFields += ",[CreatedOn]";
                    DBFieldValues += ",@CreatedOn";
                    sqlcmd.Parameters.Add("@CreatedOn", SqlDbType.DateTime);
                    sqlcmd.Parameters["@CreatedOn"].Value = DateTime.UtcNow.ToString("dd-MMM-yyyy HH:mm:ss");

                    DBFields += ",[CreatedBy]";
                    DBFieldValues += ",@CreatedBy";
                    sqlcmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                    sqlcmd.Parameters["@CreatedBy"].Value = oAttnJob.CreatedBy;

                    sqlcmd.CommandText = "Insert into tblTAttendanceJob (" + DBFields + ") values (" + DBFieldValues + ")";
                    try
                    {
                        sqlcmd.ExecuteNonQuery();
                    }
                    catch (SqlException SQlInsertEx)
                    {
                        ErrMsg = SQlInsertEx.Message;
                        success = false;
                    }
                    catch (Exception insertex)
                    {
                        ErrMsg = insertex.Message;
                        success = false;
                    }
                    finally
                    {
                        if (sqlconn.State == ConnectionState.Open)
                            sqlconn.Close();
                    }
                }

            }
            catch (SqlException ODBCEx)
            {
                ErrMsg = ODBCEx.Message;
                success = false;
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message;
                success = false;
            }
            finally
            {
                if (success == false)
                    throw new Exception("SQLServerDBAttendanceManager.InsertAttendanceData( " + ErrMsg + " )");
            }
        }
    }
}
