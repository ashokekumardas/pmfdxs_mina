﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMFDServiceDAL.SQLServerDB
{
    public class SQLServerDBDeviceStatusManager
    {
        public DeviceStatus GetOpenDeviceStatusByCPUID(string ConnStr, string cpuid)
        {
            DeviceStatus result = new DeviceStatus();
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                sqlconn.Open();
                if (sqlconn.State == ConnectionState.Open)
                {
                    SqlCommand sqlcmd = new SqlCommand("Select top 1 [CPUID],[DeviceAdd],[EstbCode],[Type],[SubType],[DeviceStat],[IPAddress],[CardPrefix],[EmpIDPrefix]," +
                                                        "[GPRSSignal],[GPRSOperator],[GPRSTower],[SIMNo],[UncapRec],[EnrolledUser],[TotalUser],[TotalTemplate],[Firmware]," +
                                                        "[FirmwareID],[TimeZone],[LastSyncWithServer] from tblTDeviceStatus where [CPUID]='" + cpuid + "' " +
                                                        " And [ClosedOn] is NULL ", sqlconn);
                    using (IDataReader reader = sqlcmd.ExecuteReader())
                    {
                        if (reader.Read())
                            result = CommonDBLayer.GetDeviceStatusFromReader(reader);
                    }
                }
                return result;
            }
        }

        public void InsertDeviceStatus(string ConnStr, DeviceStatus DS)
        {
            bool success = false;
            string ErrorMessage = string.Empty;
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                sqlconn.Open();
                if (sqlconn.State == ConnectionState.Open)
                {
                    string DBFields = "", DBFieldValues = "";
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.Connection = sqlconn;

                    DBFields = "[CPUID]";
                    DBFieldValues = "@CPUID";
                    sqlcmd.Parameters.Add("@CPUID", SqlDbType.VarChar, 30);
                    sqlcmd.Parameters["@CPUID"].Value = DS.CPUID;

                    DBFields += ",[DeviceAdd]";
                    DBFieldValues += ",@DeviceAdd";
                    sqlcmd.Parameters.Add("@DeviceAdd", SqlDbType.VarChar, 2);
                    sqlcmd.Parameters["@DeviceAdd"].Value = DS.DeviceAdd;

                    DBFields += ",[EstbCode]";
                    DBFieldValues += ",@EstbCode";
                    sqlcmd.Parameters.Add("@EstbCode", SqlDbType.VarChar, 8);
                    sqlcmd.Parameters["@EstbCode"].Value = DS.EstbCode;

                    DBFields += ",[Type]";
                    DBFieldValues += ",@Type";
                    sqlcmd.Parameters.Add("@Type", SqlDbType.VarChar, 2);
                    sqlcmd.Parameters["@Type"].Value = DS.DeviceType;

                    DBFields += ",[SubType]";
                    DBFieldValues += ",@SubType";
                    sqlcmd.Parameters.Add("@SubType", SqlDbType.VarChar, 8);
                    sqlcmd.Parameters["@SubType"].Value = DS.SubType;

                    DBFields += ",[DeviceStat]";
                    DBFieldValues += ",@DeviceStatus";
                    sqlcmd.Parameters.Add("@DeviceStatus", SqlDbType.VarChar, 32);
                    sqlcmd.Parameters["@DeviceStatus"].Value = DS.DeviceStat;

                    DBFields += ",[IPAddress]";
                    DBFieldValues += ",@IPAddress";
                    sqlcmd.Parameters.Add("@IPAddress", SqlDbType.VarChar, 20);
                    sqlcmd.Parameters["@IPAddress"].Value = DS.IPAddress;

                    DBFields += ",[CardPrefix]";
                    DBFieldValues += ",@CardPrefix";
                    sqlcmd.Parameters.Add("@CardPrefix", SqlDbType.VarChar, 5);
                    sqlcmd.Parameters["@CardPrefix"].Value = DS.CardPrefix;

                    DBFields += ",[EmpIDPrefix]";
                    DBFieldValues += ",@EmpIDPrefix";
                    sqlcmd.Parameters.Add("@EmpIDPrefix", SqlDbType.VarChar, 12);
                    sqlcmd.Parameters["@EmpIDPrefix"].Value = DS.EmpIDPrefix;

                    DBFields += ",[GPRSSignal]";
                    DBFieldValues += ",@GPRSSignal";
                    sqlcmd.Parameters.Add("@GPRSSignal", SqlDbType.VarChar, 5);
                    sqlcmd.Parameters["@GPRSSignal"].Value = DS.GPRSSignal;

                    DBFields += ",[GPRSOperator]";
                    DBFieldValues += ",@GPRSOperator";
                    sqlcmd.Parameters.Add("@GPRSOperator", SqlDbType.VarChar, 30);
                    sqlcmd.Parameters["@GPRSOperator"].Value = DS.GPRSOperator;

                    DBFields += ",[GPRSTower]";
                    DBFieldValues += ",@GPRSTower";
                    sqlcmd.Parameters.Add("@GPRSTower", SqlDbType.VarChar, 30);
                    sqlcmd.Parameters["@GPRSTower"].Value = DS.GPRSTower;

                    DBFields += ",[SIMNo]";
                    DBFieldValues += ",@SIMNo";
                    sqlcmd.Parameters.Add("@SIMNo", SqlDbType.VarChar, 15);
                    sqlcmd.Parameters["@SIMNo"].Value = DS.SIMNo;

                    DBFields += ",[UncapRec]";
                    DBFieldValues += ",@UncapRec";
                    sqlcmd.Parameters.Add("@UncapRec", SqlDbType.Int);
                    sqlcmd.Parameters["@UncapRec"].Value = DS.UncapRec;

                    DBFields += ",[EnrolledUser]";
                    DBFieldValues += ",@EnrolledUser";
                    sqlcmd.Parameters.Add("@EnrolledUser", SqlDbType.Int);
                    sqlcmd.Parameters["@EnrolledUser"].Value = DS.EnrolledUser;


                    DBFields += ",[TotalUser]";
                    DBFieldValues += ",@TotalUser";
                    sqlcmd.Parameters.Add("@TotalUser", SqlDbType.Int);
                    sqlcmd.Parameters["@TotalUser"].Value = DS.TotalUser;

                    DBFields += ",[TotalTemplate]";
                    DBFieldValues += ",@TotalTemplate";
                    sqlcmd.Parameters.Add("@TotalTemplate", SqlDbType.Int);
                    sqlcmd.Parameters["@TotalTemplate"].Value = DS.TotalTemplate;

                    DBFields += ",[Firmware]";
                    DBFieldValues += ",@Firmware";
                    sqlcmd.Parameters.Add("@Firmware", SqlDbType.VarChar, 15);
                    sqlcmd.Parameters["@Firmware"].Value = DS.Firmware;

                    DBFields += ",[FirmwareID]";
                    DBFieldValues += ",@FirmwareID";
                    sqlcmd.Parameters.Add("@FirmwareID", SqlDbType.VarChar, 20);
                    sqlcmd.Parameters["@FirmwareID"].Value = DS.FirmwareID;

                    DBFields += ",[TimeZone]";
                    DBFieldValues += ",@TimeZone";
                    sqlcmd.Parameters.Add("@TimeZone", SqlDbType.VarChar, 7);
                    sqlcmd.Parameters["@TimeZone"].Value = DS.TimeZone;

                    DBFields += ",[LastSyncWithServer]";
                    DBFieldValues += ",@LastSyncWithServer";
                    sqlcmd.Parameters.Add("@LastSyncWithServer", SqlDbType.DateTime);
                    sqlcmd.Parameters["@LastSyncWithServer"].Value = DateTime.UtcNow.ToString("dd-MMM-yyyy HH:mm:ss"); //oDBTerminalStatus.TerminalSyncDate;

                    sqlcmd.CommandText = "Insert into tblTDeviceStatus (" + DBFields + ") values (" + DBFieldValues + ")";
                    try
                    {
                        sqlcmd.ExecuteNonQuery();
                        success = true;
                    }
                    catch (Exception Exception)
                    {
                        success = false;
                        ErrorMessage = Exception.Message;
                    }
                    finally
                    {
                        if (sqlconn.State == ConnectionState.Open)
                            sqlconn.Close();
                        if (success == false)
                            throw new Exception("SQLServerDBDeviceStatusManager.InsertDeviceStatus( " + ErrorMessage + " )");
                    }
                }
            }
        }

        public void UpdateDeviceStatus(string ConnStr, DeviceStatus DS)
        {
            bool success = true;
            string ErrorMessage = string.Empty;
            using (SqlConnection sqlconn = new SqlConnection(ConnStr))
            {
                try
                {
                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        string DBFields = "";
                        SqlCommand sqlcmd = new SqlCommand();
                        sqlcmd.Connection = sqlconn;

                        DBFields = "[DeviceAdd]=@DeviceAdd";
                        sqlcmd.Parameters.Add("@DeviceAdd", SqlDbType.VarChar, 2);
                        sqlcmd.Parameters["@DeviceAdd"].Value = DS.DeviceAdd;

                        DBFields += ",[EstbCode]=@EstbCode";
                        sqlcmd.Parameters.Add("@EstbCode", SqlDbType.VarChar, 8);
                        sqlcmd.Parameters["@EstbCode"].Value = DS.EstbCode;

                        DBFields += ",[Type]=@Type";
                        sqlcmd.Parameters.Add("@Type", SqlDbType.VarChar, 2);
                        sqlcmd.Parameters["@Type"].Value = DS.DeviceType;

                        DBFields += ",[SubType]=@SubType";
                        sqlcmd.Parameters.Add("@SubType", SqlDbType.VarChar, 8);
                        sqlcmd.Parameters["@SubType"].Value = DS.SubType;

                        DBFields += ",[DeviceStat]=@DeviceStatus";
                        sqlcmd.Parameters.Add("@DeviceStatus", SqlDbType.VarChar, 32);
                        sqlcmd.Parameters["@DeviceStatus"].Value = DS.DeviceStat;

                        DBFields += ",[IPAddress]=@IPAddress";
                        sqlcmd.Parameters.Add("@IPAddress", SqlDbType.VarChar, 20);
                        sqlcmd.Parameters["@IPAddress"].Value = DS.IPAddress;

                        DBFields += ",[CardPrefix]=@CardPrefix";
                        sqlcmd.Parameters.Add("@CardPrefix", SqlDbType.VarChar, 5);
                        sqlcmd.Parameters["@CardPrefix"].Value = DS.CardPrefix;

                        DBFields += ",[EmpIDPrefix]=@EmpIDPrefix";
                        sqlcmd.Parameters.Add("@EmpIDPrefix", SqlDbType.VarChar, 12);
                        sqlcmd.Parameters["@EmpIDPrefix"].Value = DS.EmpIDPrefix;

                        DBFields += ",[GPRSSignal]=@GPRSSignal";
                        sqlcmd.Parameters.Add("@GPRSSignal", SqlDbType.VarChar, 5);
                        sqlcmd.Parameters["@GPRSSignal"].Value = DS.GPRSSignal;

                        DBFields += ",[GPRSOperator]=@GPRSOperator";
                        sqlcmd.Parameters.Add("@GPRSOperator", SqlDbType.VarChar, 30);
                        sqlcmd.Parameters["@GPRSOperator"].Value = DS.GPRSOperator;

                        DBFields += ",[GPRSTower]=@GPRSTower";
                        sqlcmd.Parameters.Add("@GPRSTower", SqlDbType.VarChar, 30);
                        sqlcmd.Parameters["@GPRSTower"].Value = DS.GPRSTower;

                        DBFields += ",[SIMNo]=@SIMNo";
                        sqlcmd.Parameters.Add("@SIMNo", SqlDbType.VarChar, 15);
                        sqlcmd.Parameters["@SIMNo"].Value = DS.SIMNo;

                        DBFields += ",[UncapRec]=@UncapRec";
                        sqlcmd.Parameters.Add("@UncapRec", SqlDbType.Int);
                        sqlcmd.Parameters["@UncapRec"].Value = DS.UncapRec;

                        DBFields += ",[EnrolledUser]=@EnrolledUser";
                        sqlcmd.Parameters.Add("@EnrolledUser", SqlDbType.Int);
                        sqlcmd.Parameters["@EnrolledUser"].Value = DS.EnrolledUser;

                        DBFields += ",[TotalTemplate]=@TotalTemplate";
                        sqlcmd.Parameters.Add("@TotalTemplate", SqlDbType.Int);
                        sqlcmd.Parameters["@TotalTemplate"].Value = DS.TotalTemplate;

                        DBFields += ",[Firmware]=@Firmware";
                        sqlcmd.Parameters.Add("@Firmware", SqlDbType.VarChar, 15);
                        sqlcmd.Parameters["@Firmware"].Value = DS.Firmware;

                        DBFields += ",[FirmwareID]=@FirmwareID";
                        sqlcmd.Parameters.Add("@FirmwareID", SqlDbType.VarChar, 20);
                        sqlcmd.Parameters["@FirmwareID"].Value = DS.FirmwareID;

                        // DBFields += ",[TimeZone]=@TimeZone";// 29-Mar-2019 timezone parameter omitted for multi location
                        //sqlcmd.Parameters.Add("@TimeZone", SqlDbType.VarChar, 7);// 29-Mar-2019 timezone parameter omitted for multi location
                        // sqlcmd.Parameters["@TimeZone"].Value = DS.TimeZone;// 29-Mar-2019 timezone parameter omitted for multi location

                        DBFields += ",[LastSyncWithServer]=@LastSyncWithServer";
                        sqlcmd.Parameters.Add("@LastSyncWithServer", SqlDbType.DateTime);
                        sqlcmd.Parameters["@LastSyncWithServer"].Value = DateTime.UtcNow;  // DBTerminalStatus.TerminalSyncDate;

                        //TimeSpan ts = oDBTerminalStatus.TerminalCloesdDate - Convert.ToDateTime("01/01/0001 00:00:00");
                        //if (ts.TotalMinutes > 0)  // close the record with close date
                        //{
                        //    DBFields += ",[ClosedOn]=@ClosedOn";
                        //    sqlcmd.Parameters.Add("@ClosedOn", SqlDbType.DateTime);
                        //    sqlcmd.Parameters["@ClosedOn"].Value = oDBTerminalStatus.TerminalCloesdDate;
                        //}
                        sqlcmd.CommandText = "Update tblTDeviceStatus  set " + DBFields + " where [CPUID]='" + DS.CPUID + "' and ClosedOn is NULL";

                        sqlcmd.ExecuteNonQuery();
                    }
                }

                catch (Exception Ex)
                {
                    success = false;
                    ErrorMessage = Ex.Message;
                }
                finally
                {
                    if (sqlconn.State == ConnectionState.Open)
                        sqlconn.Close();
                    if (success == false)
                        throw new Exception("SQLServerDBDeviceStatusManager.UpdateDeviceStatus( " + ErrorMessage + " )");
                }
            }
        }

    }
}
