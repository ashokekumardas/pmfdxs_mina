﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;


namespace PMFDXSForm
{
    public class Json
    {
        public string ConvertObjectToJSon<T>(T obj)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream MS = new MemoryStream();
            ser.WriteObject(MS, obj);
            string JSonString = Encoding.UTF8.GetString(MS.ToArray());
            MS.Close();
            return JSonString;
        }

        public T ConvertJSonToObject<T>(string JSonString)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream MS = new MemoryStream(Encoding.UTF8.GetBytes(JSonString));
            T obj = (T)serializer.ReadObject(MS);
            return obj;
        }

    }
}


