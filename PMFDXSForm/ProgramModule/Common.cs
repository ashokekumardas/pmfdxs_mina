﻿using System;
using System.Diagnostics;
using System.IO;

namespace PMFDXSForm
{
    public enum EnumJobCommand
    {
        FaceTemplate = 1
    }
    public class Comm
    {
        public string GetDataFromKey(string data, string key)
        {
            string val = string.Empty;
            int start, end;
            start = data.IndexOf(key);
            if (start >= 0)
            {
                start = data.IndexOf("\"", start);
                if (start >= 0)
                {
                    end = data.IndexOf("\"", start + 1);
                    if (end > start)
                    {
                        val = data.Substring(start + 1, end - (start + 1));
                    }
                }

            }
            return val;
        }

        public string calculateCheckSum(string data)
        {
            int i, intchecksum = 0;
            string checkSum = string.Empty;
            char chr;
            try
            {
                for (i = 0; i < data.Length; i++)
                {
                    chr = data[i];
                    intchecksum ^= Convert.ToInt32(chr);
                }
                checkSum = Convert.ToString(intchecksum, 16).ToUpper();
                if (checkSum.Length < 2)
                    checkSum = "0" + checkSum;
            }
            catch (Exception ex)
            {
            }
            return checkSum;
        }

        public static string GetJobCommandFromEnum(EnumJobCommand JobCommand)
        {
            string Command = string.Empty;
            switch (JobCommand)
            {
                case EnumJobCommand.FaceTemplate:
                    Command = "F2200";
                    break;
            }
            return Command;
        }

        public void WriteLog(string logfilename, string record)
        {
            try
            {
                Checkandcreatelogfolder();
                string filename = GlobalVariables.ServiceApplicationLogFolder + "\\" + logfilename;
                if (!File.Exists(filename))
                {
                    using (StreamWriter sw = File.CreateText(filename))
                        sw.WriteLine(DateTime.Now.ToString() + " - " + record);
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filename))
                        sw.WriteLine(DateTime.Now.ToString() + " - " + record);
                }
            }
            catch (Exception e)
            { }
        }

        public void Checkandcreatelogfolder()
        {
            try
            {
                string ProcessName = Process.GetCurrentProcess().ProcessName;
                string strpath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" + ProcessName + "_Logs";
                if (!Directory.Exists(strpath))
                    Directory.CreateDirectory(strpath);
            }
            catch (Exception e)
            {
                //strerror = e.Message;
            }
        }

        internal static string TrimErrorMessage(string msg, int msgLength = 250)
        {//18-Jun-2020 this is to cut the long error message to write to log file
            if (msg.Length <= msgLength)
                return msg;
            else
                return msg.Substring(0, msgLength);// return only specified length
        }
    }
}
