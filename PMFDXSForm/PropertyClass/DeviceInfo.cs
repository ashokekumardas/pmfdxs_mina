﻿namespace PMFDXSForm
{
    public class DeviceInfo
    {
        public string sn { get; set; }
        public string edition { get; set; }
        public string firmwareVersion { get; set; }
        public string type { get; set; }
        public string alg_edition { get; set; }
        public string max_faceregist { get; set; }
        public string real_faceregist { get; set; }
        public string max_facerecord { get; set; }
        public string name { get; set; }
        public string real_facerecord { get; set; }
        public string mac { get; set; }
        public string ip { get; set; }
        public string netmask { get; set; }
        public string gateway { get; set; }
    }

    public class DEVICEINFOPARAM
    {
        public string result { get; set; }
        public string reason { get; set; }
        public DeviceInfo deviceInfo { get; set; }
    }

    public class NewDeviceInfo
    {
        public string COMMAND { get; set; }
        public DEVICEINFOPARAM DeviceInfoParam { get; set; }
    }
}
