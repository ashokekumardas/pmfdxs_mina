﻿namespace PMFDXSForm
{
    public  class ReceivedCommandData
    {
        public string COMMAND { get; set; }
        public PARAM PARAM { get; set; }
    }

    public class PARAM
    {
        public string sn { get; set; }
    }
}
