﻿
namespace PMFDXSForm
{
    partial class frmPMFDXS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.tbServerIP = new System.Windows.Forms.TextBox();
            this.btStartService = new System.Windows.Forms.Button();
            this.btTestConnection = new System.Windows.Forms.Button();
            this.PanelConnection = new System.Windows.Forms.Panel();
            this.chkTrustedConnection = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.optSQLServer = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.tbHBPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCMDPort = new System.Windows.Forms.TextBox();
            this.textBoxAnswer = new System.Windows.Forms.TextBox();
            this.PanelConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 49;
            this.label5.Text = "Server IP";
            // 
            // tbServerIP
            // 
            this.tbServerIP.Location = new System.Drawing.Point(86, 3);
            this.tbServerIP.MaxLength = 15;
            this.tbServerIP.Name = "tbServerIP";
            this.tbServerIP.Size = new System.Drawing.Size(249, 20);
            this.tbServerIP.TabIndex = 48;
            this.tbServerIP.Text = "192.168.0.94";
            // 
            // btStartService
            // 
            this.btStartService.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStartService.Location = new System.Drawing.Point(341, 171);
            this.btStartService.Name = "btStartService";
            this.btStartService.Size = new System.Drawing.Size(120, 23);
            this.btStartService.TabIndex = 47;
            this.btStartService.Text = "Start";
            this.btStartService.UseVisualStyleBackColor = true;
            this.btStartService.Click += new System.EventHandler(this.btStartService_Click);
            // 
            // btTestConnection
            // 
            this.btTestConnection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTestConnection.Location = new System.Drawing.Point(215, 171);
            this.btTestConnection.Name = "btTestConnection";
            this.btTestConnection.Size = new System.Drawing.Size(120, 23);
            this.btTestConnection.TabIndex = 46;
            this.btTestConnection.Text = "Test Connection";
            this.btTestConnection.UseVisualStyleBackColor = true;
            this.btTestConnection.Click += new System.EventHandler(this.btTestConnection_Click);
            // 
            // PanelConnection
            // 
            this.PanelConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelConnection.Controls.Add(this.chkTrustedConnection);
            this.PanelConnection.Controls.Add(this.label7);
            this.PanelConnection.Controls.Add(this.label8);
            this.PanelConnection.Controls.Add(this.label10);
            this.PanelConnection.Controls.Add(this.label11);
            this.PanelConnection.Controls.Add(this.txtPassword);
            this.PanelConnection.Controls.Add(this.txtDatabase);
            this.PanelConnection.Controls.Add(this.txtUserID);
            this.PanelConnection.Controls.Add(this.txtDSN);
            this.PanelConnection.Controls.Add(this.optSQLServer);
            this.PanelConnection.Location = new System.Drawing.Point(9, 52);
            this.PanelConnection.Name = "PanelConnection";
            this.PanelConnection.Size = new System.Drawing.Size(450, 113);
            this.PanelConnection.TabIndex = 45;
            // 
            // chkTrustedConnection
            // 
            this.chkTrustedConnection.AutoSize = true;
            this.chkTrustedConnection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTrustedConnection.Location = new System.Drawing.Point(8, 85);
            this.chkTrustedConnection.Name = "chkTrustedConnection";
            this.chkTrustedConnection.Size = new System.Drawing.Size(133, 19);
            this.chkTrustedConnection.TabIndex = 23;
            this.chkTrustedConnection.Text = "Trusted Connection";
            this.chkTrustedConnection.UseVisualStyleBackColor = true;
            this.chkTrustedConnection.Visible = false;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(241, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(241, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "User Id";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "Database";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 20);
            this.label11.TabIndex = 17;
            this.label11.Text = "Server";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(325, 64);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(118, 20);
            this.txtPassword.TabIndex = 16;
            this.txtPassword.Text = "abc@123";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(78, 60);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(127, 20);
            this.txtDatabase.TabIndex = 15;
            this.txtDatabase.Text = "ProComm";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(325, 38);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(118, 20);
            this.txtUserID.TabIndex = 14;
            this.txtUserID.Text = "sa";
            // 
            // txtDSN
            // 
            this.txtDSN.Location = new System.Drawing.Point(78, 34);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(127, 20);
            this.txtDSN.TabIndex = 13;
            this.txtDSN.Text = "DESKTOP-M3GD1IS\\SQL2014";
            // 
            // optSQLServer
            // 
            this.optSQLServer.Checked = true;
            this.optSQLServer.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optSQLServer.Location = new System.Drawing.Point(8, 3);
            this.optSQLServer.Name = "optSQLServer";
            this.optSQLServer.Size = new System.Drawing.Size(133, 20);
            this.optSQLServer.TabIndex = 21;
            this.optSQLServer.TabStop = true;
            this.optSQLServer.Text = "MS SQL Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "HB Port";
            // 
            // tbHBPort
            // 
            this.tbHBPort.Location = new System.Drawing.Point(335, 26);
            this.tbHBPort.Name = "tbHBPort";
            this.tbHBPort.Size = new System.Drawing.Size(124, 20);
            this.tbHBPort.TabIndex = 43;
            this.tbHBPort.Text = "9964";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "CMD Port";
            // 
            // tbCMDPort
            // 
            this.tbCMDPort.Location = new System.Drawing.Point(88, 26);
            this.tbCMDPort.Name = "tbCMDPort";
            this.tbCMDPort.Size = new System.Drawing.Size(127, 20);
            this.tbCMDPort.TabIndex = 41;
            this.tbCMDPort.Text = "9960";
            // 
            // textBoxAnswer
            // 
            this.textBoxAnswer.Location = new System.Drawing.Point(9, 200);
            this.textBoxAnswer.Multiline = true;
            this.textBoxAnswer.Name = "textBoxAnswer";
            this.textBoxAnswer.ReadOnly = true;
            this.textBoxAnswer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAnswer.Size = new System.Drawing.Size(452, 221);
            this.textBoxAnswer.TabIndex = 50;
            // 
            // frmPMFDXS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 423);
            this.Controls.Add(this.textBoxAnswer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbServerIP);
            this.Controls.Add(this.btStartService);
            this.Controls.Add(this.btTestConnection);
            this.Controls.Add(this.PanelConnection);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbHBPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCMDPort);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPMFDXS";
            this.Text = "PMFDXS";
            this.PanelConnection.ResumeLayout(false);
            this.PanelConnection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbServerIP;
        private System.Windows.Forms.Button btStartService;
        private System.Windows.Forms.Button btTestConnection;
        private System.Windows.Forms.Panel PanelConnection;
        private System.Windows.Forms.CheckBox chkTrustedConnection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.RadioButton optSQLServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbHBPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCMDPort;
        private System.Windows.Forms.TextBox textBoxAnswer;
    }
}

