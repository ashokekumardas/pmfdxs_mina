﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Windows.Forms;
using Com.FirstSolver.Splash;  //31-Mar-2021 // direct data entry to tblTattendanceJob , MSMQ only for raw file write
using Mina.Core.Session;
using Mina.Filter.Codec;
using Mina.Transport.Socket;
using PMFDServiceDAL;
using System.Diagnostics;

namespace PMFDXSForm
{
    public partial class frmPMFDXS : Form
    {
        private static readonly AttributeKey KEY_SERIALNUMBER = new AttributeKey(typeof(frmPMFDXS), "SerialNumber");

        private const int DeviceCodePage = 936;
        private bool IsServerRunning = false;
        private AsyncSocketAcceptor TcpServer = null;
        private AsyncDatagramAcceptor UdpServer = null;
        private AsyncSocketAcceptor TcpNewServer = null;
        private AsyncDatagramAcceptor UdpNewServer = null;
        private string ProcessName = string.Empty;

        public frmPMFDXS()
        {
            InitializeComponent();
        }

        private void btTestConnection_Click(object sender, EventArgs e)
        {
            string connectionString = GetDBConnectionString();
            //MessageBox.Show(connectionString);
            try
            {
                if (optSQLServer.Checked == true)
                {
                    SqlConnection testConnection = new SqlConnection(connectionString);
                    testConnection.Open();
                    testConnection.Close();
                }
                else
                {

                }
                MessageBox.Show("Connection Test Ok");

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        #region DATABASE
        internal string GetDBConnectionString()
        {
            string trustedConnection = "No", connectionString = "";
            if (chkTrustedConnection.Checked == true)
                trustedConnection = "Yes";
            if (this.optSQLServer.Checked == true)// sql server connection
                if (trustedConnection == "Yes")
                    connectionString = "Data Source=" + this.txtDSN.Text + ";Initial Catalog=" + this.txtDatabase.Text + ";Trusted_Connection=" + trustedConnection + ";";
                else
                    connectionString = "Data Source=" + this.txtDSN.Text + ";Database=" + this.txtDatabase.Text + ";Integrated Security=False;Persist Security Info=False;User ID=" + txtUserID.Text + ";Password=" + txtPassword.Text + ";Max Pool Size=200; Pooling=True;Connect Timeout=120";
            else // 27-Aug-2018 Oracle connection
            {
                //   connectionString = "Data Source=" + txtDSN.Text + ";User Id=" + txtUserID.Text + ";Password=" + txtPassword.Text + ";SID=" + txtDatabase.Text;
                connectionString = "Data Source=(DESCRIPTION=" +
                "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + this.txtDSN.Text + ")(PORT=1521)))"
               + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + this.txtDatabase.Text + ")));"
               + "User Id=" + txtUserID.Text + ";Password=" + txtPassword.Text + ";";
            }

            //'Data Source=122.160.53.175,3341\sqlexpress;Initial Catalog=SmartFaceDB;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=abc@123;Max Pool Size=200; Pooling=True;Connect Timeout=120'
            //<add key="DatabasePath" value ="Data Source=software-laptop;User Id=system;Password=oracle;"/>-->
            //  <!--<add key="DatabaseType" value ="ORACLE"/>-->
            return connectionString;
        }
        #endregion
        #region New Implementation
        public void StartService()
        {
            try
            {
                ProcessName = Process.GetCurrentProcess().ProcessName;
                string IPAddress = tbServerIP.Text;
                string CMDPort = tbCMDPort.Text;
                string HBport = tbHBPort.Text;
                string CmdNewPort ="9910";
                string HBNewport = "9911";
                try
                {
                    StartServiceInPort(IPAddress, CMDPort, CmdNewPort);
                    StartUDPServiceInPort(IPAddress, HBport, HBNewport);// start udp service

                    /////////////////////////  check the MSMQ data read  /////////////////////
                    //HandleMSMQ HCMQ = new HandleMSMQ(ProcessName);
                    //timerDataMQ = new System.Timers.Timer(10000);
                    //timerDataMQ.Elapsed += new ElapsedEventHandler(HCMQ.ReadFromMessageQ);
                    //timerDataMQ.Interval = 5000;
                    //timerDataMQ.Enabled = true;
                    /////////////////////////  clean the old log & raw files //////////////////
                    //EventLogger EL = new EventLogger();
                    //timerCleanFiles = new Timer(12 * 60 * 60 * 1000);
                    //timerCleanFiles.Elapsed += new ElapsedEventHandler(EL.CleanLogFiles);
                    //timerCleanFiles.Enabled = true;

                    System.Threading.Thread ListenThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartListening));
                    ListenThread.Start();

                    System.Threading.Thread ListenNewDeviceThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartNewServiceListening));// Listerservice for new devices VF 1000      )
                    ListenNewDeviceThread.Start();

                    System.Threading.Thread ListenUDPThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartUDPService)); // UDP service listen
                    ListenUDPThread.Start();// UDP service listen

                    System.Threading.Thread ListenUDPNewThread = new System.Threading.Thread(new System.Threading.ThreadStart(StartNewUDPService)); // UDP new device service listen
                    ListenUDPNewThread.Start();//new UDP service listen

                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Service started at " + IPAddress + "," + CMDPort);
                    //for (; ; ) { System.Threading.Thread.Sleep(1000 * 60 * 5); } // comment for Service
                }
                catch (Exception exp)
                {
                    new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Failed to start HB service at " + IPAddress + "," + HBport + ", Reason " + exp.Message);
                }
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartService Error " + ex.Message.ToString());
            }
        }
        public void StartServiceInPort(string IPaddress, string port, string NewDevicePort)
        {
            try
            {
                TcpServer = new AsyncSocketAcceptor();
                TcpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));
                // new device                
                TcpNewServer = new AsyncSocketAcceptor();
                TcpNewServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory(false, null)));

                if (IsServerRunning)
                {   // Stop listening
                    if (TcpServer != null)
                    {
                        TcpServer.Dispose();
                        TcpServer = null;
                    }
                    IsServerRunning = false;
                }
                else
                {
                    // Bind listening port，Start listening
                    TcpServer.Bind(new IPEndPoint(IPAddress.Parse(IPaddress), int.Parse(port)));
                    TcpNewServer.Bind(new IPEndPoint(IPAddress.Parse(IPaddress), int.Parse(NewDevicePort)));
                    IsServerRunning = true;
                }
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartService( " + ex.Message.ToString() + ")"); }
        }

        public void StartUDPServiceInPort(string ServerIP, string HBport, string NewDeviceHBPort)
        {
            try
            {
                UdpServer = new AsyncDatagramAcceptor();
                UdpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));
                // new device
                UdpNewServer = new AsyncDatagramAcceptor();
                UdpNewServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory(false, null)));
                UdpNewServer.SessionConfig.ReuseAddress = true;

                // bind listener with address and port
                UdpServer.Bind(new IPEndPoint(IPAddress.Parse(ServerIP), int.Parse(HBport)));
                UdpNewServer.Bind(new IPEndPoint(IPAddress.Parse(ServerIP), int.Parse(NewDeviceHBPort)));
            }
            catch (Exception ex) { new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartUDPServiceInPort( " + ex.Message.ToString() + ")"); }
        }
        #region NORMAL FACE READER COMMUNICATION

        public void StartListening()
        {
            string DeviceSN = string.Empty;
            AttendanceManager AttnM = new AttendanceManager();  // create the object of attendance manager for attendance data insert
            JobManager JobM = new JobManager();  // create the instance of the job manager
            List<Job> ListOfJobs = new List<Job>();
            Comm cm = new Comm();

            try
            {
                // Open listening                
                //TcpServer.SessionOpened += (o, ea) =>
                //{
                //    if (!checkBoxPassiveEncryption.Checked)
                //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
                //};
                TcpServer.MessageReceived += (o, ea) =>
                {   // Display the received string                    
                    string Message = (string)ea.Message;
                    //Console.WriteLine(Message);
                    this.Invoke(new Action(() =>
                    {
                        textBoxAnswer.AppendText(Message + "\r\n");
                    }));
                    if (Message.StartsWith("PostRecord"))
                    {   // Extract the serial number and save
                        string SerialNumber = cm.GetDataFromKey(Message, "sn");
                        ea.Session.SetAttribute(KEY_SERIALNUMBER, SerialNumber);
                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                        // Reply is ready to receive attendance records
                        // Need to upload photos
                        ea.Session.Write("Return(result=\"success\" postphoto=\"true\")");

                    }
                    else if (Message.StartsWith("Record"))
                    {   // Get device serial number
                        string SerialNumber = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);

                        // Processing card point data
                        if (DeviceSN.Length < 16)
                            DeviceSN = DeviceSN.PadLeft(16, '0');
                        else if (DeviceSN.Length > 16)
                            DeviceSN = DeviceSN.Substring(DeviceSN.Length - 16);
                        // implemented from the version 1.2.1 for direct insert to db ////////////
                        Attendance Attn = new Attendance();
                        Attn.CPUID = DeviceSN;
                        Attn.Command = 1001;
                        Attn.CommandNo = 0;
                        Attn.CommandData = Message;
                        Attn.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                        Attn.SendTo = 'S';
                        if (!AttnM.Insert(Attn)) // tblTAttendanceJob failed
                        {
                            // failed to insert into attednance file.log the data in some text file
                            //so same data will come again. So no need to write to DNP file
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed Attn Insert to tblTAttendanceJob " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + ")");
                        }
                        else // tblTAttendanceJob table success
                        {
                            try
                            {
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message); // write to raw file
                                                                                                                   //string attendancedata = Answer + "~" + DeviceSN; ;
                                                                                                                   //if (HMSMQ.WriteToMessageQ(attendancedata, true))
                                                                                                                   // Server response
                                ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                            }
                            catch (Exception ex)
                            {
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                            }
                        }
                    }
                    else if (Message.StartsWith("PostEmployee"))
                    {   // Prepare to upload personnel
                        ea.Session.Write("Return(result=\"success\")");
                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                    }
                    else if (Message.StartsWith("Employee"))
                    {   // Processing personnel data
                        Job job = new Job();
                        job.CPUID = DeviceSN;
                        job.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                        job.Command = Comm.GetJobCommandFromEnum(EnumJobCommand.FaceTemplate);//face template job
                        if (JobM.ProcessJobReply(job, Message.ToString()))    // send the job for process the reply
                        {
                            // Server response
                            ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                        }
                        else
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->ProcessJobReply-Employee, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                    }
                    else if (Message.StartsWith("GetRequest"))
                    {   // Issue an order
                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                        ListOfJobs.Clear(); // clear the old jobs if present                        
                        JobM.LoadDateTimeSetCommandInToList(DeviceSN, ListOfJobs);  // load the set date time command it will act as dummy place holder
                        JobM.LoadGetDeviceInformationCommandInToList(DeviceSN, ListOfJobs);// load the get device information command to get the device status information  
                        if (JobM.GetNewJobsForThisCPUID(DeviceSN, ListOfJobs, 10) == false)  // load the jobs in the list of objects
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->GetNewJobsForThisCPUID, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                        if (ListOfJobs.Count > 0)// some job present then send the first job // date time set job
                            ea.Session.Write(ListOfJobs[0].CommandData); // // date time set job sent to sevice first job
                    }
                    else if (Message.StartsWith("Return"))
                    {
                        if (ListOfJobs.Count > 0)  // there is jobs in the list so close it and update to the table and remove from list
                        {
                            if (JobM.ProcessJobReply(ListOfJobs[0], Message.ToString()))    // send the job for process the reply
                                ListOfJobs.RemoveAt(0); // remove the first element from the list that is already done
                            else
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->ProcessJobReply->Return, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                            // JobM.UpdateFinishedJob(ListOfJobs[0]);                            
                        }
                        if (ListOfJobs.Count == 0)// no more jobs in the list
                        {
                            ea.Session.Close(true);  // send the end of job to the device 

                        }
                        else
                            ea.Session.Write(ListOfJobs[0].CommandData);
                    }
                    else if (Message.StartsWith("Quit"))
                    {   // Disconnect
                        ea.Session.Close(true);
                    }
                };
                TcpServer.ExceptionCaught += (o, ea) =>
                {   // Thread safety
                    cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "exception , SN- " + DeviceSN + " " + ea.Exception.Message.ToString());
                    ea.Session.Close(true);
                };
            }
            catch (Exception ex)
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", ex.Message.ToString());

            }
        }



        /////////////// UDP service event//////////////////////////
        private void StartUDPService()
        {
            UdpServer.SessionConfig.ReuseAddress = true;
            UdpServer.MessageReceived += (s, ea) =>
            {
                string Message = (string)ea.Message;
                Console.WriteLine(Message);
            };

            UdpServer.ExceptionCaught += (s, ea) =>
            {
                Console.WriteLine("Exception: " + ea.Exception.Message.ToString() + "\r\n");
                ea.Session.Close(true);
            };
        }
        #endregion NORMAL FACE READER COMMUNICATION

        #region NEW FACE READERS JSON COMUNICATION VF1000

        private void StartNewServiceListening()
        {
            string DeviceSN = string.Empty;
            bool IsSuccess = false;
            AttendanceManager AttnM = new AttendanceManager();  // create the object of attendance manager for attendance data insert
            JobManager JobM = new JobManager();  // create the instance of the job manager
            List<Job> ListOfJobs = new List<Job>();
            //TcpNewServer.SessionOpened += (o, ea) =>
            //{
            //    if (!checkBoxPassiveEncryption.Checked)
            //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
            //};
            Comm cm = new Comm();
            TcpNewServer.MessageReceived += (o, ea) =>
            {
                string Message = (string)ea.Message;
                this.Invoke(new Action(() =>
                {
                    textBoxAnswer.AppendText(Message + "\r\n");
                }));
                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", Message);  // write received data
                COMMAND_PCIR_TYPE A = Message.FromJsonTo<COMMAND_PCIR_TYPE>();
                if (string.Equals(A.COMMAND, "RecogniseResult"))                 // Attendance data and Post Employeee data
                {
                    IsSuccess = false;  //  default value
                    if (Message.IndexOf("face_data") < 0) // Attendance data , other wise post employee data
                    {
                        Attendance Attn = new AttendanceManager().ProcessAttendanceDataFromJson(Message);//convert json string into attendance data                  
                        if (!AttnM.Insert(Attn)) // tblTAttendanceJob failed
                        {
                            // failed to insert into attednance file.log the data in some text file
                            //so same data will come again. So no need to write to DNP file
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening(Error- Failed Attn Insert to tblTAttendanceJob " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + ")");
                        }
                        else // tblTAttendanceJob table success
                        {
                            try
                            {
                                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message); // write to raw file
                                IsSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                            }
                        }
                    }
                    else  // post employee record, insert to job table
                    {
                        Job job = new Job();
                        job.CPUID = DeviceSN;
                        job.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                        job.Command = "V2200"; Comm.GetJobCommandFromEnum(EnumJobCommand.FaceTemplate);//face template job
                        job.CommandNo = 0;
                        job.CommandData = Message;
                        job.SendTo = 'S';
                        //if (JobM.Insert(job))    //post employee record inserted  to tblTjob success                                                 
                        //    IsSuccess = true;   // Reply data received successfully                        
                        //else
                        //    cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening->Post employee record, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                    }
                    if (IsSuccess == true) //// Server response success
                    {
                        try
                        {
                            REPLY_PCIR_TYPE M = new REPLY_PCIR_TYPE();
                            M.RETURN = "RecogniseResult";
                            M.PARAM = new PARAM_REPLY_PCIR_TYPE();
                            M.PARAM.result = "success";
                            ea.Session.Write(M.ToJsonString()); ;   //  data received successfully
                        }
                        catch (Exception ex)
                        {
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening(Error- Failed to write to raw file " + DeviceSN + " " + Comm.TrimErrorMessage(Message) + " " + ex.Message + ")");
                        }
                    }
                }
                else if (string.Equals(A.COMMAND, "GetRequest"))//  request for job from device
                {
                    ReceivedCommandData RCD = new Json().ConvertJSonToObject<ReceivedCommandData>(Message);// convert Json to class object
                    DeviceSN = RCD.PARAM.sn;// get device serial no
                    ListOfJobs.Clear(); // clear the old jobs if present                        
                    JobM.LoadNewDeviceDateTimeSetCommandInToList(DeviceSN, ListOfJobs);  // load the set date time command it will act as dummy place holder
                    JobM.LoadGetNewDeviceInformationCommandInToList(DeviceSN, ListOfJobs);// load the get device information command to get the device status information  
                    if (JobM.GetNewJobsForThisCPUID(DeviceSN, ListOfJobs, 1) == false)  // load the jobs in the list of objects
                        cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- StartNewServiceListening->GetNewJobsForThisCPUID, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                    if (ListOfJobs.Count == 0)// some job present then send the first job // date time set job
                    {
                        ea.Session.Close(true);
                    }
                    else
                    {
                        ea.Session.Write(ListOfJobs[0].CommandData); //send the job to the device
                    }
                }
                else if (string.Equals(A.COMMAND, "Return"))
                {
                    if (ListOfJobs.Count > 0)  // there is jobs in the list so close it and update to the table and remove from list
                    {
                        if (JobM.ProcessNewDeviceJobReply(ListOfJobs[0], Message.ToString()))    // send the job for process the reply in Json string
                            ListOfJobs.RemoveAt(0); // remove the first element from the list that is already done
                        else
                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "StartNewServiceListening->ProcessJobReply->Return, SN- " + DeviceSN + ", Error - " + Comm.TrimErrorMessage(JobM.GetErrorReport()));
                    }
                    if (ListOfJobs.Count == 0)// no more jobs in the list
                    {
                        ea.Session.Close(true);  // send the end of job to the device 
                    }
                    else
                        ea.Session.Write(ListOfJobs[0].CommandData);
                }
                else
                {
                    ea.Session.Close(true);
                }
            };
            TcpNewServer.SessionClosed += (o, ea) =>
            {
            };
            TcpNewServer.ExceptionCaught += (o, ea) =>
            {
               // Console.WriteLine("Exception: " + ea.Exception.Message.ToString());
                ea.Session.Close(true);
            };
        }
        // New UDP Service
        private void StartNewUDPService()
        {

            UdpNewServer.MessageReceived += (s, ea) =>
            {
                string Message = (string)ea.Message;
                //    textBoxAnswer.AppendText(Message + "\r\n");
                //                        if (checkBoxTask.Checked)
                {
                    /*{
                        "RETURN":"DevStatus",
                        "PARAM":{
                                                        "result":"success/fail",
                            "reason":"",
                            "command":"PostRequest"

                        }
                    }*/
                    //ea.Session.Write("PostRequest()");
                    REPLY_HEARTBEAT_TYPE rht = new REPLY_HEARTBEAT_TYPE();
                    rht.PARAM = new PARAM_REPLY_HEARTBEAT_TYPE();
                    ea.Session.Write(JsonHelper.ToJsonString(rht));
                    //Console.WriteLine("Send: " + JsonHelper.ToJsonString(rht));
                }
            };

            UdpNewServer.ExceptionCaught += (s, ea) =>
            {
                new Comm().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Exception: " + ea.Exception.Message.ToString() + ")");
                ea.Session.Close(true);
            };
        }
        private void InsertAttendanceDataToDB(Attendance Attn) // attendance data to be inserted to the database table
        {

        }
        #endregion NEW FACE READERS JSON COMUNICATION VF1000
        private void btStartService_Click(object sender, EventArgs e)
        {
            if (btStartService.Text == "Start")
            {
                
                StartService();
                IsServerRunning = true;
                btStartService.Text = "Stop";
            }
            else
            {
                TcpServer.Dispose();
                TcpServer = null;
                UdpServer.Dispose();
                UdpServer = null;
                TcpNewServer.Dispose();
                TcpNewServer = null;
                UdpNewServer.Dispose();
                UdpNewServer = null;
                btStartService.Text = "Start";
                IsServerRunning = false;
            }
        }
            #endregion New Implementation
            /*
                    private void btStartService_Click(object sender, EventArgs e)
                    {
                        string DeviceSN = string.Empty;
                        AttendanceManager AttnM = new AttendanceManager();  // create the object of attendance manager for attendance data insert
                        JobManager JobM = new JobManager();  // create the instance of the job manager
                        List<Job> ListOfJobs = new List<Job>();

                        try
                        {
                            if (IsServerRunning)
                            {   // Stop listening
                                if (TcpServer != null)
                                {
                                    TcpServer.Dispose();
                                    TcpServer = null;
                                }
                                btStartService.Text = "Start";
                                IsServerRunning = false;
                            }
                            else
                            {
                                // Open listening
                                Comm cm = new Comm();
                                TcpServer = new AsyncSocketAcceptor();
                                //TcpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory(false, null)));
                                TcpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));
                                //TcpServer.SessionOpened += (o, ea) =>
                                //{
                                //    if (!checkBoxPassiveEncryption.Checked)
                                //        FaceReaderProtocolCodecFactory.EnablePassiveEncryption(ea.Session, true, SM2ServerInfo);
                                //};
                                TcpServer.MessageReceived += (o, ea) =>
                                {   // Display the received string
                                    string Message = (string)ea.Message;
                                    this.Invoke(new Action(() =>
                                    {
                                        textBoxAnswer.AppendText(Message + "\r\n");
                                    }));
                                    if (Message.StartsWith("PostRecord"))
                                    {   // Extract the serial number and save
                                        string SerialNumber = cm.GetDataFromKey(Message, "sn");
                                        ea.Session.SetAttribute(KEY_SERIALNUMBER, SerialNumber);
                                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                                        // Reply is ready to receive attendance records
                                        // Need to upload photos
                                        ea.Session.Write("Return(result=\"success\" postphoto=\"true\")");

                                    }
                                    else if (Message.StartsWith("Record"))
                                    {   // Get device serial number
                                        string SerialNumber = (string)ea.Session.GetAttribute(KEY_SERIALNUMBER);

                                        // Processing card point data
                                        if (DeviceSN.Length < 16)
                                            DeviceSN = DeviceSN.PadLeft(16, '0');
                                        else if (DeviceSN.Length > 16)
                                            DeviceSN = DeviceSN.Substring(DeviceSN.Length - 16);
                                        new Common().WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message);// 17-Jun-2020 write to log file v 1.2.4
                                                                                                                                    // implemented from the version 1.2.1 for direct insert to db ////////////
                                        Attendance Attn = new Attendance();
                                        Attn.CPUID = DeviceSN;
                                        Attn.Command = 1001;
                                        Attn.CommandNo = 0;
                                        Attn.CommandData = Message;
                                        Attn.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                                        Attn.SendTo = 'S';
                                        if (!AttnM.Insert(Attn)) // tblTAttendanceJob failed
                                        {
                                            // failed to insert into attednance file.log the data in some text file
                                            //so same data will come again. So no need to write to DNP file
                                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed Attn Insert to tblTAttendanceJob " + DeviceSN + " " + Common.TrimErrorMessage(Message) + ")");
                                        }
                                        else // tblTAttendanceJob table success
                                        {
                                            try
                                            {
                                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + "-" + DeviceSN + ".raw", Message); // write to raw file
                                                                                                                                   //string attendancedata = Answer + "~" + DeviceSN; ;
                                                                                                                                   //if (HMSMQ.WriteToMessageQ(attendancedata, true))
                                                                                                                                   // Server response
                                                ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                                            }
                                            catch (Exception ex)
                                            {
                                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "OnServerTaskProcess(Error- Failed to write to raw file " + DeviceSN + " " + Common.TrimErrorMessage(Message) + " " + ex.Message + ")");
                                            }
                                        }
                                    }
                                    else if (Message.StartsWith("PostEmployee"))
                                    {   // Prepare to upload personnel
                                        ea.Session.Write("Return(result=\"success\")");
                                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                                    }
                                    else if (Message.StartsWith("Employee"))
                                    {   // Processing personnel data
                                        Job job = new Job();
                                        job.CPUID = DeviceSN;
                                        job.RefNo = DateTime.UtcNow.ToString("ddMMyyHHmmssfff");
                                        job.Command = Common.GetJobCommandFromEnum(EnumJobCommand.FaceTemplate);//face template job
                                        if (JobM.ProcessJobReply(job, Message.ToString()))    // send the job for process the reply
                                        {
                                            // Server response
                                            ea.Session.Write("Return(result=\"success\")");   // Reply data received successfully
                                        }
                                        else
                                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->ProcessJobReply-Employee, SN- " + DeviceSN + ", Error - " + Common.TrimErrorMessage(JobM.GetErrorReport()));
                                    }
                                    else if (Message.StartsWith("GetRequest"))
                                    {   // Issue an order
                                        DeviceSN = cm.GetDataFromKey(Message, "sn");
                                        ListOfJobs.Clear(); // clear the old jobs if present                        
                                        JobM.LoadDateTimeSetCommandInToList(DeviceSN, ListOfJobs);  // load the set date time command it will act as dummy place holder
                                        JobM.LoadGetDeviceInformationCommandInToList(DeviceSN, ListOfJobs);// load the get device information command to get the device status information  
                                        if (JobM.GetNext25JobsForThisCPUID(DeviceSN, ListOfJobs) == false)  // load the jobs in the list of objects
                                            cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->GetNext25JobsForThisCPUID, SN- " + DeviceSN + ", Error - " + Common.TrimErrorMessage(JobM.GetErrorReport()));
                                        if (ListOfJobs.Count > 0)// some job present then send the first job // date time set job
                                            ea.Session.Write(ListOfJobs[0].CommandData); // // date time set job sent to sevice first job
                                    }
                                    else if (Message.StartsWith("Return"))
                                    {
                                        if (ListOfJobs.Count > 0)  // there is jobs in the list so close it and update to the table and remove from list
                                        {
                                            if (JobM.ProcessJobReply(ListOfJobs[0], Message.ToString()))    // send the job for process the reply
                                                ListOfJobs.RemoveAt(0); // remove the first element from the list that is already done
                                            else
                                                cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "Process- OnServerTaskProcess->ProcessJobReply->Return, SN- " + DeviceSN + ", Error - " + Common.TrimErrorMessage(JobM.GetErrorReport()));
                                            // JobM.UpdateFinishedJob(ListOfJobs[0]);                            
                                        }
                                        if (ListOfJobs.Count == 0)// no more jobs in the list
                                            ea.Session.Close(true);  // send the end of job to the device 
                                        else
                                            ea.Session.Write(ListOfJobs[0].CommandData);
                                    }
                                    else if (Message.StartsWith("Quit"))
                                    {   // Disconnect
                                        ea.Session.Close(true);
                                    }
                                };

                                TcpServer.ExceptionCaught += (o, ea) =>
                                {   // Thread safety
                                    cm.WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", "exception , SN- " + DeviceSN + " " + ea.Exception.Message.ToString());
                                    ea.Session.Close(true);
                                };

                                // Bind listening port，Start listening
                                TcpServer.Bind(new IPEndPoint(IPAddress.Parse(tbServerIP.Text), int.Parse(tbCMDPort.Text)));
                                IsServerRunning = true;
                                btStartService.Text = "Stop";
                                StartUDPService(tbServerIP.Text, tbHBPort.Text);//
                            }
                        }
                        catch (Exception ex)
                        {
                            new Common().WriteLog(DateTime.Now.ToString("ddMMyyyy") + ".log", ex.Message.ToString());

                        }
                    }

                    private void StartUDPService(string ServerIP,string UDPPort)
                    {
                        if (IsServerRunning)
                        {  
                            if (UdpServer != null)
                            {
                                UdpServer.Dispose();
                                UdpServer = null;
                            }

                            IsServerRunning = false;
                        }
                        else
                        {                
                            UdpServer = new AsyncDatagramAcceptor();
                            UdpServer.FilterChain.AddLast("codec", new ProtocolCodecFilter(new FaceIdProtocolCodecFactory(DeviceCodePage, true, false)));

                            UdpServer.SessionConfig.ReuseAddress = true;
                            UdpServer.MessageReceived += (s, ea) =>
                            {   
                                string Message = (string)ea.Message;
                                textBoxAnswer.BeginInvoke(new Action(() =>
                                {
                                    textBoxAnswer.AppendText(Message + "\r\n");
                                    ea.Session.Write("PostRequest()");
                                }));
                            };

                            UdpServer.ExceptionCaught += (s, ea) =>
                            {   
                                textBoxAnswer.BeginInvoke(new Action(() =>
                                {
                                    textBoxAnswer.AppendText("Exception: " + ea.Exception.Message.ToString() + "\r\n");
                                }));

                                ea.Session.Close(true);
                            };
                            UdpServer.Bind(new IPEndPoint(IPAddress.Parse(ServerIP), int.Parse(UDPPort)));
                            IsServerRunning = true;

                        }
                    }*/
        }
}

