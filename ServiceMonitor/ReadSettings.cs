﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace PMFDServiceMonitor
{
    public partial class ReadSettings
    {
        public static string getConnectionString()
        {

            string dbPath = System.Configuration.ConfigurationManager.AppSettings["DatabasePath"];
            return dbPath;
        }

        /// <summary>
        /// get server port to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerCMDPort()
        {

            string serverPort = string.Empty;
            if (System.Configuration.ConfigurationManager.AppSettings["ServerCMDPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerCMDPort"];
            return serverPort;
        }

        /// <summary>
        /// get server port to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerHBPort()
        {

            string serverPort = string.Empty ;
            if (System.Configuration.ConfigurationManager.AppSettings["ServerHBPort"] != null)
                serverPort = System.Configuration.ConfigurationManager.AppSettings["ServerHBPort"];
            return serverPort;
        }
        /// <summary>
        /// get server IP to listen from clients
        /// </summary>
        /// <returns></returns>
        public static string getServerIP()
        {
            string serverIP = "127.0.0.0";
            if (System.Configuration.ConfigurationManager.AppSettings["ServerIP"] != null)
                serverIP = System.Configuration.ConfigurationManager.AppSettings["ServerIP"];
            return serverIP;
        }

        public static string GetOtherConfigValue(string ConfigFileNmewithPath, string key)
        {
            string value=string.Empty;
            try
            {
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = ConfigFileNmewithPath; // @"C:\test\ConfigurationManager.exe.config";
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                value = config.AppSettings.Settings[key].Value;
            }
            catch (Exception ex) { throw new Exception(ex.StackTrace.ToString()); }
            return value;
        }
    }

}

