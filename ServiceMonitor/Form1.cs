﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using PMFDServiceMonitor;
using System.Data.OracleClient;

namespace ServiceMonitor
{
    public partial class Form1 : Form
    {
        private string LastError = string.Empty;
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern uint GetShortPathName([MarshalAs(UnmanagedType.LPTStr)] string lpszLongPath,
            [MarshalAs(UnmanagedType.LPTStr)]StringBuilder lpszShortPath, uint cchBuffer);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadServicesFromRegistry();
        }

        /// <summary>
        /// Add the sevice 
        /// </summary>
        /// <param name="ServiceName"></param>
        /// <param name="ServiceApplicationWithShortPath"></param>
        /// <returns></returns>
        private int AddService(string ServiceName, string ServiceApplicationWithShortPath)
        {
            try
            {
                ServiceInstaller srvinstaller = new ServiceInstaller();
                srvinstaller.InstallService(ServiceApplicationWithShortPath, ServiceName, ServiceName);
                return 0;
            }

            catch (Exception objException)
            {
                LastError = objException.Message;
                return -1;
            }
        }

        public static string ToShortPathName(string longName)
        {
            uint bufferSize = 256;

            /* don´t allocate stringbuilder here but outside of the function for fast access , believe me, it will speed up your code */
            StringBuilder shortNameBuffer = new StringBuilder((int)bufferSize);
            uint result = GetShortPathName(longName, shortNameBuffer, bufferSize);
            return shortNameBuffer.ToString();
        }

        private void btDeleteService_Click(object sender, EventArgs e)
        {
            if (lvProfileList.Items.Count > 0) // some names are present
            {
                if (MessageBox.Show("Are you sure to delete this service", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    long i;
                    int listi = 0;
                    System.ServiceProcess.ServiceController controller = new System.ServiceProcess.ServiceController(lvProfileList.SelectedItems[0].Text);
                    try
                    {
                        if (controller.Status.ToString() != "Stopped")
                        {
                            controller.Stop();
                            for (i = 0; i < 20000000; i++) ;
                        }
                        /////remove from the service list ///////////////////
                        ServiceInstaller srvinst = new ServiceInstaller();
                        srvinst.UnInstallService(controller.ServiceName );
                        string ServiceApplicationFile=  ToShortPathName(Application.StartupPath)+"\\"+controller.ServiceName +".exe";
                        string ServiceApplicationConfigFile = ToShortPathName(Application.StartupPath) + "\\" + controller.ServiceName + ".exe.config";
                        System.IO.File.Delete(ServiceApplicationFile);  // delete the application file after service uninstall
                        System.IO.File.Delete(ServiceApplicationConfigFile);  // delete the application config file after service uninstall
                        MessageBox.Show("Restart the Service Monitor application");
                        Application.Exit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An Error occurred Stopping service:\n" + controller.DisplayName + ex.Message);
                    }
                }
            }
        }

        private void btTestConnection_Click(object sender, EventArgs e)
        {
            string connectionString = GetDBConnectionString();
            //MessageBox.Show(connectionString);
            try
            {
                if (optSQLServer.Checked == true)
                {
                    SqlConnection testConnection = new SqlConnection(connectionString);
                    testConnection.Open();
                    testConnection.Close();
                }
                else
                {
                    OracleConnection OraConn = new OracleConnection(connectionString);
                    OraConn.Open();
                    OraConn.Close();
                }
                MessageBox.Show("Connection Test Ok");
                btSaveProfile.Enabled = true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                btSaveProfile.Enabled = false;
            }
            finally
            {
                
            }

        }

        private void btSaveProfile_Click(object sender, EventArgs e)
        {
            string connectionstring = GetDBConnectionString(), ServiceFilePathName = string.Empty; // get the db connection string

            if (tbServiceName.Text.Length == 0)
            {
                MessageBox.Show("Service Profile cannot be blank");
                return;
            }
            if (tbCMDPort.Text.Length == 0)
            {
                MessageBox.Show("Command port cannot be blank");
                return;
            }
            if (tbHBPort.Text.Length == 0)
            {
                MessageBox.Show("Heart beat port cannot be blank");
                return;
            }
            if (tbServerIP.Text.Length == 0)
            {
                MessageBox.Show("Define Server IP address");
                return;
            }
            if (tbCMDPortSession.Text.Length == 0)
            {
                MessageBox.Show("Session Command port cannot be blank");
                return;
            }
            if (tbHBPortSession.Text.Length == 0)
            {
                MessageBox.Show("Session Heart beat port cannot be blank");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            ////////////update config value ///////////////////////////////
            string UpdAction = string.Empty;
            try
            {
                UpdAction = "ServerCMDPort";
                UpdateSetting("ServerCMDPort", tbCMDPort.Text);
                UpdAction = "ServerHBPort";
                UpdateSetting("ServerHBPort", tbHBPort.Text);
                UpdAction = "ServerHBPort";
                UpdateSetting("ServerHBPort", tbHBPort.Text);
                UpdAction = "DatabaseType";
                UpdateSetting("DatabaseType", (optSQLServer.Checked == true ? "SQLSERVER" : "ORACLE")); // 27-Aug-2018 set database type
                UpdAction = "DatabasePath";
                UpdateSetting("DatabasePath", connectionstring);
                UpdAction = "ServerIP";
                UpdateSetting("ServerIP", tbServerIP.Text);
                UpdAction = "ServerCMDNewPort";
                UpdateSetting("ServerCMDNewPort", tbCMDPortSession.Text);// new device CMD port
                UpdAction = "ServerHBNewPort";
                UpdateSetting("ServerHBNewPort", tbHBPortSession.Text); // new device HB port
                UpdAction = "InsertFaceTemplate";
                UpdateSetting("InsertFaceTemplate", chkInsertFaceTemplate.Checked==true?"Yes":"No"); // Insert face template on enrolment
                UpdAction = "InsertFacePhoto";
                UpdateSetting("InsertFacePhoto", chkInsertFacePhoto.Checked == true ? "Yes" : "No"); // Insert face photo from attendance data      
            }
            catch (Exception UpdEx) { MessageBox.Show("Error in updating value " + UpdAction + " " + UpdEx.Message); return; }
            /////////////////// copy the application and config file //////////////
            try
            {
                string shortFilePath = ToShortPathName(Application.StartupPath);
                // Use Path class to manipulate file and directory paths.
                string fileName = "PurpleMoorFDService.exe";
                string trgFileName = "PMFDService_" + tbServiceName.Text + ".exe";
                string sourceFile = System.IO.Path.Combine(shortFilePath, fileName);
                string destFile = System.IO.Path.Combine(shortFilePath, trgFileName);
                ServiceFilePathName = destFile;
                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);
                // copy app settings file
                fileName = "PMFDServiceMonitor.exe.config";
                trgFileName = "PMFDService_" + tbServiceName.Text + ".exe.config";
                sourceFile = System.IO.Path.Combine(shortFilePath, fileName);
                destFile = System.IO.Path.Combine(shortFilePath, trgFileName);
                System.IO.File.Copy(sourceFile, destFile, true);
                if (AddService("PMFDService_" + tbServiceName.Text, ServiceFilePathName) == 0)  // add the service and run
                {
                    LoadServicesFromRegistry(); // dispaly the service name
                    RefreshFields();
                    Cursor.Current = Cursors.Default; // normal mouse pointer
                    MessageBox.Show("Service Installed");
                }
                else
                {
                    Cursor.Current = Cursors.Default; // normal mouse pointer
                    MessageBox.Show(LastError);
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default; // normal mouse pointer
                MessageBox.Show(ex.Message);
            }
        }

        private string GetDBConnectionString()
        {
            string trustedConnection = "No", connectionString = "";
            if (chkTrustedConnection.Checked == true)
                trustedConnection = "Yes";
            if (this.optSQLServer.Checked == true)// sql server connection
                if (trustedConnection == "Yes")
                    connectionString = "Data Source=" + this.txtDSN.Text + ";Initial Catalog=" + this.txtDatabase.Text + ";Trusted_Connection=" + trustedConnection + ";";
                else
                    connectionString = "Data Source=" + this.txtDSN.Text + ";Database=" + this.txtDatabase.Text + ";Integrated Security=False;Persist Security Info=False;User ID=" + txtUserID.Text + ";Password=" + txtPassword.Text + ";Max Pool Size=200; Pooling=True;Connect Timeout=120";
            else // 27-Aug-2018 Oracle connection
            {
             //   connectionString = "Data Source=" + txtDSN.Text + ";User Id=" + txtUserID.Text + ";Password=" + txtPassword.Text + ";SID=" + txtDatabase.Text;
                connectionString="Data Source=(DESCRIPTION=" +
                "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + this.txtDSN.Text + ")(PORT=1521)))"
               + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + this.txtDatabase.Text + ")));" 
               + "User Id="+txtUserID.Text +";Password="+txtPassword.Text+";";
            }

            //'Data Source=122.160.53.175,3341\sqlexpress;Initial Catalog=SmartFaceDB;Integrated Security=False;Persist Security Info=False;User ID=sa;Password=abc@123;Max Pool Size=200; Pooling=True;Connect Timeout=120'
            //<add key="DatabasePath" value ="Data Source=software-laptop;User Id=system;Password=oracle;"/>-->
            //  <!--<add key="DatabaseType" value ="ORACLE"/>-->
            return connectionString;
        }
        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// This function will retrive all the dxs services from the registry
        /// </summary>
        private void LoadServicesFromRegistry()
        {
            this.lvProfileList.Items.Clear();
            try
            {
                System.ServiceProcess.ServiceController controller;
                System.ServiceProcess.ServiceController[] services;
                services = System.ServiceProcess.ServiceController.GetServices();
                for (int i = 0; i < services.Length; i++)
                {
                    controller = services[i];
                    if (controller.DisplayName.IndexOf("PMFDService_") >= 0)  // service is a FDX service dispaly the details
                    {
                        try
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.Text = controller.DisplayName;
                            lvi.SubItems.Add(ReadSettings.GetOtherConfigValue(Application.StartupPath + "\\" + controller.ServiceName + ".exe.config", "ServerCMDPort"));
                            lvi.SubItems.Add(ReadSettings.GetOtherConfigValue(Application.StartupPath + "\\" + controller.ServiceName + ".exe.config", "ServerHBPort"));
                            lvi.SubItems.Add(GetServiceStatus(controller.ServiceName));
                            this.lvProfileList.Items.Add(lvi);
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.StackTrace.ToString()); }
        }

        private string GetServiceStatus(string serviceName)
        {
            System.ServiceProcess.ServiceController controller = new System.ServiceProcess.ServiceController(serviceName);
            return controller.Status.ToString();
        }
        private void RefreshFields()
        {
            tbCMDPort.Text = "9920"; tbHBPort.Text = "9924"; tbServiceName.Text = ""; tbServerIP.Text = "";
            btSaveProfile.Enabled = false;
            txtDatabase.Text = ""; txtDSN.Text = ""; txtPassword.Text = ""; txtUserID.Text = "";
            optOracle.Checked = true;
        }

        /// <summary>
        /// load all the services and status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btRefresh_Click(object sender, EventArgs e)
        {
            LoadServicesFromRegistry(); // loads install services
        }

        private void lvProfileList_Click(object sender, EventArgs e)
        {
            btStartStop.Enabled = false;
            if (lvProfileList.Items.Count > 0) // some names are present
            {
                string ServiceStatus = GetServiceStatus(lvProfileList.SelectedItems[0].Text);
                if (ServiceStatus == "Running")
                {
                    btStartStop.Text = "Stop";
                    btStartStop.Enabled = true;
                }
                else if (ServiceStatus == "Stopped")
                {
                    btStartStop.Text = "Start";
                    btStartStop.Enabled = true;
                }
            }
        }

        private void btStartStop_Click(object sender, EventArgs e)
        {
            btStartStop.Enabled = false;
            if (lvProfileList.Items.Count > 0) // some names are present
            {
                System.ServiceProcess.ServiceController controller = new System.ServiceProcess.ServiceController(lvProfileList.SelectedItems[0].Text);
                try
                {
                    string ServiceStatus = GetServiceStatus(controller.ServiceName);
                    if (ServiceStatus == "Running")
                    {
                        controller.Stop();
                        for (int i = 0; i < 20000000; i++) ;

                    }
                    else if (ServiceStatus == "Stopped")
                    {
                        controller.Start();
                        for (int i = 0; i < 20000000; i++) ;

                    }
                    LoadServicesFromRegistry(); // loads install services
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void optOracle_CheckedChanged(object sender, EventArgs e)
        {
            if (optOracle.Checked == true)
                txtDatabase.Visible = true;
            else
                txtDatabase.Visible = true;
        }
    }
}
