﻿namespace ServiceMonitor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tbServiceName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btDeleteService = new System.Windows.Forms.Button();
            this.lvProfileList = new System.Windows.Forms.ListView();
            this.ProfileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CMDPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HPPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ProfileStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.tbCMDPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbHBPort = new System.Windows.Forms.TextBox();
            this.PanelConnection = new System.Windows.Forms.Panel();
            this.chkTrustedConnection = new System.Windows.Forms.CheckBox();
            this.optOracle = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.optSQLServer = new System.Windows.Forms.RadioButton();
            this.btTestConnection = new System.Windows.Forms.Button();
            this.btSaveProfile = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbServerIP = new System.Windows.Forms.TextBox();
            this.btRefresh = new System.Windows.Forms.Button();
            this.btStartStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbHBPortSession = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbCMDPortSession = new System.Windows.Forms.TextBox();
            this.chkInsertFaceTemplate = new System.Windows.Forms.CheckBox();
            this.chkInsertFacePhoto = new System.Windows.Forms.CheckBox();
            this.PanelConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbServiceName
            // 
            this.tbServiceName.Location = new System.Drawing.Point(123, 15);
            this.tbServiceName.Margin = new System.Windows.Forms.Padding(4);
            this.tbServiceName.MaxLength = 15;
            this.tbServiceName.Name = "tbServiceName";
            this.tbServiceName.Size = new System.Drawing.Size(331, 22);
            this.tbServiceName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Service Name";
            // 
            // btDeleteService
            // 
            this.btDeleteService.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDeleteService.Location = new System.Drawing.Point(463, 526);
            this.btDeleteService.Margin = new System.Windows.Forms.Padding(4);
            this.btDeleteService.Name = "btDeleteService";
            this.btDeleteService.Size = new System.Drawing.Size(160, 28);
            this.btDeleteService.TabIndex = 5;
            this.btDeleteService.Text = "Delete";
            this.btDeleteService.UseVisualStyleBackColor = true;
            this.btDeleteService.Click += new System.EventHandler(this.btDeleteService_Click);
            // 
            // lvProfileList
            // 
            this.lvProfileList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ProfileName,
            this.CMDPort,
            this.HPPort,
            this.ProfileStatus});
            this.lvProfileList.FullRowSelect = true;
            this.lvProfileList.GridLines = true;
            this.lvProfileList.HideSelection = false;
            this.lvProfileList.Location = new System.Drawing.Point(16, 340);
            this.lvProfileList.Margin = new System.Windows.Forms.Padding(4);
            this.lvProfileList.MultiSelect = false;
            this.lvProfileList.Name = "lvProfileList";
            this.lvProfileList.Size = new System.Drawing.Size(605, 179);
            this.lvProfileList.TabIndex = 6;
            this.lvProfileList.UseCompatibleStateImageBehavior = false;
            this.lvProfileList.View = System.Windows.Forms.View.Details;
            this.lvProfileList.SelectedIndexChanged += new System.EventHandler(this.lvProfileList_Click);
            // 
            // ProfileName
            // 
            this.ProfileName.Text = "Name";
            this.ProfileName.Width = 150;
            // 
            // CMDPort
            // 
            this.CMDPort.Text = "CMD Port";
            this.CMDPort.Width = 80;
            // 
            // HPPort
            // 
            this.HPPort.Text = "HB Port";
            this.HPPort.Width = 80;
            // 
            // ProfileStatus
            // 
            this.ProfileStatus.Text = "Status";
            this.ProfileStatus.Width = 80;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 74);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "CMD Port";
            // 
            // tbCMDPort
            // 
            this.tbCMDPort.Location = new System.Drawing.Point(159, 70);
            this.tbCMDPort.Margin = new System.Windows.Forms.Padding(4);
            this.tbCMDPort.Name = "tbCMDPort";
            this.tbCMDPort.Size = new System.Drawing.Size(100, 22);
            this.tbCMDPort.TabIndex = 7;
            this.tbCMDPort.Text = "9920";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(330, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "HB Port";
            // 
            // tbHBPort
            // 
            this.tbHBPort.Location = new System.Drawing.Point(452, 70);
            this.tbHBPort.Margin = new System.Windows.Forms.Padding(4);
            this.tbHBPort.Name = "tbHBPort";
            this.tbHBPort.Size = new System.Drawing.Size(100, 22);
            this.tbHBPort.TabIndex = 9;
            this.tbHBPort.Text = "9924";
            // 
            // PanelConnection
            // 
            this.PanelConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelConnection.Controls.Add(this.chkTrustedConnection);
            this.PanelConnection.Controls.Add(this.optOracle);
            this.PanelConnection.Controls.Add(this.label7);
            this.PanelConnection.Controls.Add(this.label8);
            this.PanelConnection.Controls.Add(this.label10);
            this.PanelConnection.Controls.Add(this.label11);
            this.PanelConnection.Controls.Add(this.txtPassword);
            this.PanelConnection.Controls.Add(this.txtDatabase);
            this.PanelConnection.Controls.Add(this.txtUserID);
            this.PanelConnection.Controls.Add(this.txtDSN);
            this.PanelConnection.Controls.Add(this.optSQLServer);
            this.PanelConnection.Location = new System.Drawing.Point(16, 160);
            this.PanelConnection.Margin = new System.Windows.Forms.Padding(4);
            this.PanelConnection.Name = "PanelConnection";
            this.PanelConnection.Size = new System.Drawing.Size(599, 139);
            this.PanelConnection.TabIndex = 32;
            // 
            // chkTrustedConnection
            // 
            this.chkTrustedConnection.AutoSize = true;
            this.chkTrustedConnection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTrustedConnection.Location = new System.Drawing.Point(11, 105);
            this.chkTrustedConnection.Margin = new System.Windows.Forms.Padding(4);
            this.chkTrustedConnection.Name = "chkTrustedConnection";
            this.chkTrustedConnection.Size = new System.Drawing.Size(164, 23);
            this.chkTrustedConnection.TabIndex = 23;
            this.chkTrustedConnection.Text = "Trusted Connection";
            this.chkTrustedConnection.UseVisualStyleBackColor = true;
            this.chkTrustedConnection.Visible = false;
            // 
            // optOracle
            // 
            this.optOracle.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optOracle.Location = new System.Drawing.Point(325, 10);
            this.optOracle.Margin = new System.Windows.Forms.Padding(4);
            this.optOracle.Name = "optOracle";
            this.optOracle.Size = new System.Drawing.Size(116, 25);
            this.optOracle.TabIndex = 22;
            this.optOracle.Text = "Oracle";
            this.optOracle.CheckedChanged += new System.EventHandler(this.optOracle_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(321, 79);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 25);
            this.label7.TabIndex = 20;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(321, 47);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 22);
            this.label8.TabIndex = 19;
            this.label8.Text = "User Id";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 74);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 25);
            this.label10.TabIndex = 18;
            this.label10.Text = "Database";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 44);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 25);
            this.label11.TabIndex = 17;
            this.label11.Text = "Server";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(433, 79);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(156, 22);
            this.txtPassword.TabIndex = 16;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(104, 74);
            this.txtDatabase.Margin = new System.Windows.Forms.Padding(4);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(168, 22);
            this.txtDatabase.TabIndex = 15;
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(433, 47);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(156, 22);
            this.txtUserID.TabIndex = 14;
            // 
            // txtDSN
            // 
            this.txtDSN.Location = new System.Drawing.Point(104, 42);
            this.txtDSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(168, 22);
            this.txtDSN.TabIndex = 13;
            // 
            // optSQLServer
            // 
            this.optSQLServer.Checked = true;
            this.optSQLServer.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optSQLServer.Location = new System.Drawing.Point(11, 4);
            this.optSQLServer.Margin = new System.Windows.Forms.Padding(4);
            this.optSQLServer.Name = "optSQLServer";
            this.optSQLServer.Size = new System.Drawing.Size(177, 25);
            this.optSQLServer.TabIndex = 21;
            this.optSQLServer.TabStop = true;
            this.optSQLServer.Text = "MS SQL Server";
            // 
            // btTestConnection
            // 
            this.btTestConnection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTestConnection.Location = new System.Drawing.Point(295, 305);
            this.btTestConnection.Margin = new System.Windows.Forms.Padding(4);
            this.btTestConnection.Name = "btTestConnection";
            this.btTestConnection.Size = new System.Drawing.Size(160, 28);
            this.btTestConnection.TabIndex = 37;
            this.btTestConnection.Text = "Test Connection";
            this.btTestConnection.UseVisualStyleBackColor = true;
            this.btTestConnection.Click += new System.EventHandler(this.btTestConnection_Click);
            // 
            // btSaveProfile
            // 
            this.btSaveProfile.Enabled = false;
            this.btSaveProfile.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveProfile.Location = new System.Drawing.Point(463, 305);
            this.btSaveProfile.Margin = new System.Windows.Forms.Padding(4);
            this.btSaveProfile.Name = "btSaveProfile";
            this.btSaveProfile.Size = new System.Drawing.Size(160, 28);
            this.btSaveProfile.TabIndex = 38;
            this.btSaveProfile.Text = "Save Profile";
            this.btSaveProfile.UseVisualStyleBackColor = true;
            this.btSaveProfile.Click += new System.EventHandler(this.btSaveProfile_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 40;
            this.label5.Text = "Server IP";
            // 
            // tbServerIP
            // 
            this.tbServerIP.Location = new System.Drawing.Point(123, 42);
            this.tbServerIP.Margin = new System.Windows.Forms.Padding(4);
            this.tbServerIP.MaxLength = 15;
            this.tbServerIP.Name = "tbServerIP";
            this.tbServerIP.Size = new System.Drawing.Size(331, 22);
            this.tbServerIP.TabIndex = 39;
            // 
            // btRefresh
            // 
            this.btRefresh.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRefresh.Location = new System.Drawing.Point(125, 526);
            this.btRefresh.Margin = new System.Windows.Forms.Padding(4);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(160, 28);
            this.btRefresh.TabIndex = 41;
            this.btRefresh.Text = "Refresh";
            this.btRefresh.UseVisualStyleBackColor = true;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // btStartStop
            // 
            this.btStartStop.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStartStop.Location = new System.Drawing.Point(295, 526);
            this.btStartStop.Margin = new System.Windows.Forms.Padding(4);
            this.btStartStop.Name = "btStartStop";
            this.btStartStop.Size = new System.Drawing.Size(160, 28);
            this.btStartStop.TabIndex = 42;
            this.btStartStop.Text = "Start";
            this.btStartStop.UseVisualStyleBackColor = true;
            this.btStartStop.Click += new System.EventHandler(this.btStartStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(330, 104);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 17);
            this.label1.TabIndex = 46;
            this.label1.Text = "HB Port (Session)";
            // 
            // tbHBPortSession
            // 
            this.tbHBPortSession.Location = new System.Drawing.Point(453, 100);
            this.tbHBPortSession.Margin = new System.Windows.Forms.Padding(4);
            this.tbHBPortSession.Name = "tbHBPortSession";
            this.tbHBPortSession.Size = new System.Drawing.Size(100, 22);
            this.tbHBPortSession.TabIndex = 45;
            this.tbHBPortSession.Text = "9911";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 104);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 17);
            this.label6.TabIndex = 44;
            this.label6.Text = "CMD Port (Session)";
            // 
            // tbCMDPortSession
            // 
            this.tbCMDPortSession.Location = new System.Drawing.Point(159, 100);
            this.tbCMDPortSession.Margin = new System.Windows.Forms.Padding(4);
            this.tbCMDPortSession.Name = "tbCMDPortSession";
            this.tbCMDPortSession.Size = new System.Drawing.Size(100, 22);
            this.tbCMDPortSession.TabIndex = 43;
            this.tbCMDPortSession.Text = "9910";
            // 
            // chkInsertFaceTemplate
            // 
            this.chkInsertFaceTemplate.AutoSize = true;
            this.chkInsertFaceTemplate.Location = new System.Drawing.Point(16, 131);
            this.chkInsertFaceTemplate.Name = "chkInsertFaceTemplate";
            this.chkInsertFaceTemplate.Size = new System.Drawing.Size(254, 21);
            this.chkInsertFaceTemplate.TabIndex = 47;
            this.chkInsertFaceTemplate.Text = "Insert Face Template On Enrolment";
            this.chkInsertFaceTemplate.UseVisualStyleBackColor = true;
            // 
            // chkInsertFacePhoto
            // 
            this.chkInsertFacePhoto.AutoSize = true;
            this.chkInsertFacePhoto.Checked = true;
            this.chkInsertFacePhoto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInsertFacePhoto.Location = new System.Drawing.Point(295, 131);
            this.chkInsertFacePhoto.Name = "chkInsertFacePhoto";
            this.chkInsertFacePhoto.Size = new System.Drawing.Size(287, 21);
            this.chkInsertFacePhoto.TabIndex = 48;
            this.chkInsertFacePhoto.Text = "Insert Face Photo From Attendance Data";
            this.chkInsertFacePhoto.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 563);
            this.Controls.Add(this.chkInsertFacePhoto);
            this.Controls.Add(this.chkInsertFaceTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbHBPortSession);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbCMDPortSession);
            this.Controls.Add(this.btStartStop);
            this.Controls.Add(this.btRefresh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbServerIP);
            this.Controls.Add(this.btSaveProfile);
            this.Controls.Add(this.btTestConnection);
            this.Controls.Add(this.PanelConnection);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbHBPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCMDPort);
            this.Controls.Add(this.lvProfileList);
            this.Controls.Add(this.btDeleteService);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbServiceName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Monitor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PanelConnection.ResumeLayout(false);
            this.PanelConnection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbServiceName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btDeleteService;
        private System.Windows.Forms.ListView lvProfileList;
        private System.Windows.Forms.ColumnHeader ProfileName;
        private System.Windows.Forms.ColumnHeader CMDPort;
        private System.Windows.Forms.ColumnHeader HPPort;
        private System.Windows.Forms.ColumnHeader ProfileStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCMDPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbHBPort;
        private System.Windows.Forms.Panel PanelConnection;
        private System.Windows.Forms.CheckBox chkTrustedConnection;
        private System.Windows.Forms.RadioButton optOracle;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.RadioButton optSQLServer;
        private System.Windows.Forms.Button btTestConnection;
        private System.Windows.Forms.Button btSaveProfile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbServerIP;
        private System.Windows.Forms.Button btRefresh;
        private System.Windows.Forms.Button btStartStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHBPortSession;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbCMDPortSession;
        private System.Windows.Forms.CheckBox chkInsertFaceTemplate;
        private System.Windows.Forms.CheckBox chkInsertFacePhoto;
    }
}

