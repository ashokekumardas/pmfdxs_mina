﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceMonitor
{
    [Serializable()]
    public class ServiceInfo
    {
        private string _DisplayName;

        public string DisplayName
        {
            get
            {
                return _DisplayName;
            }
            set { _DisplayName = value; }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private string _ListName;

        public string ListName
        {
            get { return _ServiceName + (_Computer.Length != 0 ? " on " + _Computer : ""); }
            set { _ListName = value; }
        }


        private string _ServiceName;

        public string ServiceName
        {
            get { return _ServiceName; }
            set { _ServiceName = value; }
        }

        private string _Computer = "";

        public string Computer
        {
            get { return _Computer; }
            set { _Computer = value; }
        }

    }
}
